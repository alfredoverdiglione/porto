﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Porto.Model;
using Porto.Model.Utenti;
using Porto.Persistence;
using Porto.Persistence.Interfacce;
using Porto.Controller;
using Porto.View;

namespace Porto
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool debug = false;
            if (!debug)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                //Componenti del nostro programma.
                DefaultController controller = new DefaultController(GestorePorti.GetInstance(), GestoreUtenti.GetInstance());
                MainForm form = new MainForm(GestoreUtenti.GetInstance(), controller, controller, controller);





                Application.Run(form);
            }
            else
            {
                #region Per_Creare_Il_File_Dat
                MockGestorePersistenza mock = new MockGestorePersistenza();
                List<Model.Porto> porti =mock.LoadPorti("");
                List<Utente> utenti = mock.LoadUtenti("");

                BinaryGestorePersistenza bin = new BinaryGestorePersistenza();
                bin.SavePorti(porti, "porti.dat");
                bin.SaveUtenti(utenti, "utenti.dat");
                Console.WriteLine("Ok");


                #endregion
            }
        }
    }
}
