﻿using System;
using System.Collections.Generic;
using Porto.Model;
using Porto.Model.Utenti;
using Porto.Controller.Interfacce;

namespace Porto.Controller
{
    public class DefaultController: DipendenteAmministrativoController,AssistenteDiTerraController, ClienteController, BasicController
    {
       private readonly IGestorePorti _gestorePorti;
       private readonly IGestoreUtenti _gestoreUtenti;



        public DefaultController(IGestorePorti gestorePorti, IGestoreUtenti gestoreUtenti)
        {
            if (gestoreUtenti == null || gestorePorti == null)
                throw new ArgumentException("Uno dei due parametri di inizializzazione è nullo!");
            _gestorePorti = gestorePorti;
            _gestoreUtenti = gestoreUtenti;

            
        }


        public Utente LogIn(string username, string password)
        {
            if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password))
                throw new ArgumentException("Parametri non validi!");
            List<Utente> toCheck = _gestoreUtenti.UtentiGestiti;
            foreach (Utente u in toCheck)
                if (u.Username.Equals(username) && u.Password.Equals(password))
                {
                   return u;
                }
            /**Nel caso non trovi gli utenti non posso far altro che ritornare null*/
            return null;
        }

        public bool ModificaAttività(string adminCF, Attività a, string newDescrizione, List<AssistenteDiTerra> newOperators)
        {
            if (String.IsNullOrEmpty(adminCF) || a == null || String.IsNullOrEmpty(newDescrizione) || newOperators == null)
                throw new ArgumentException("Uno dei parametri di utilizzo è non valido!");
            RimuoviAttività(adminCF, a);
            CreaAttività(adminCF,newDescrizione,a.Segnalazione,newOperators);
            SalvaStato();
            return true;

        }

        public bool ConfermaPrenotazione(Prenotazione daConfermare)
        {
            if (daConfermare == null || daConfermare.StatoPrenotazione == StatoPrenotazione.Confermata)
                throw new ArgumentException("Parametri/o di chiamata non validi!");
            daConfermare.Conferma();
            SalvaStato();
            return true;
        }

        public bool EliminaSegnalazione(Model.Porto porto, Segnalazione segnalazione)
        {
            /*Le altre condizioni vengono verificate internamente dai vari metodi che compongono il programma*/
            if (porto == null || segnalazione == null)
                throw new ArgumentException("Uno o più parametri sono nulli!");

            foreach (Model.Porto port in _gestorePorti.PortiGestiti)
            {
                if (port.Nome == porto.Nome)
                {
                    port.RimuoviSegnalazione(segnalazione);
                    foreach (Dipendente dipendenteMemory in _gestoreUtenti.Dipendenti)
                        if (dipendenteMemory.LavoraPresso.Nome == porto.Nome)
                            dipendenteMemory.LavoraPresso.Segnalazioni.Remove(segnalazione);
                    break;
                }
            }

            SalvaStato();
            return true;
        }

        public bool CreaAttività(String adminCF, string descrizione, Segnalazione s, List<AssistenteDiTerra> assistenti)
        {

            /*Le altre condizioni vengono verificate internamente dai vari metodi che compongono il programma*/
            if (String.IsNullOrEmpty(adminCF) || s==null || String.IsNullOrEmpty(descrizione) || assistenti == null)
                throw new ArgumentException("Uno dei parametri di utilizzo è non valido!");

            foreach (Utente u in _gestoreUtenti.UtentiGestiti)
            {
                if (u.CodiceFiscale == adminCF)
                {
                    DipendenteAmministrativo admin = (DipendenteAmministrativo)u;
                    _gestorePorti.OttieniPortoPerDipendente(admin).CreaAttività(descrizione, s, assistenti);
                    
                    RefreshAttivitàStatus();
                    break;
                }
            }


            SalvaStato();

            return true;
        }

        public bool RimuoviAttività(String adminCF,Attività attività)
        {
            /*Le altre condizioni vengono verificate internamente dai vari metodi che compongono il programma*/
            if (String.IsNullOrEmpty(adminCF) || attività == null)
                throw new ArgumentException("Uno dei parametri di utilizzo è non valido!");
            foreach (Utente u in _gestoreUtenti.UtentiGestiti)
            {
                if (u.CodiceFiscale == adminCF)
                {
                    DipendenteAmministrativo admin = (DipendenteAmministrativo)u;
                    _gestorePorti.OttieniPortoPerDipendente(admin).RimuoviAttività(attività);
                    RefreshAttivitàStatus();
                    break;
                }
            }

            SalvaStato();
            return true;
        }

        private void RefreshAttivitàStatus()
        {
           foreach(Utente u in _gestoreUtenti.UtentiGestiti)
            {
                if(u is AssistenteDiTerra)
                {
                    AssistenteDiTerra assistente = (AssistenteDiTerra)u;
                    assistente.Attività.Clear();
                    assistente.Attività.AddRange(_gestorePorti.OttieniPortoPerDipendente(assistente).OttieniAttivitàPerAssistente(assistente));
                }
            }
        }

        public void ModificaBarca(Barca daModificare,String nuovaTarga, String nuovoNome)
        {
            if (daModificare == null || String.IsNullOrEmpty(nuovaTarga) || String.IsNullOrEmpty(nuovoNome))
                throw new ArgumentException("Uno dei parametri di configurazione è invalido!");
            foreach (Utente utente in GestoreUtenti.UtentiGestiti)
            {
                if (utente is Cliente)
                {
                    Cliente cliente = (Cliente)utente;
                    if (daModificare.Proprietario.CodiceFiscale == cliente.CodiceFiscale)
                        foreach (Barca barca in cliente.Barche)
                        {
                            if (barca.Targa == daModificare.Targa)
                            {
                                barca.Targa = nuovaTarga;
                                barca.Nome = nuovoNome;
                            }
                        }
                }
            }
            SalvaStato();
        }

        public bool CancellaPrenotazione(Prenotazione p)
        {
            if (p == null)
                throw new ArgumentException("Cosa vuoi cancellare?");
            foreach (Model.Porto porto in GestorePorti.PortiGestiti)
            {
                if (porto.Nome.Equals(p.PostoBarca.Porto.Nome))
                    foreach (PostoBarca postoBarca in porto.PostiBarca)
                    {
                        if (postoBarca.Id.Equals(p.PostoBarca.Id))
                        { 
                            postoBarca.RimuoviPrenotazione(p);
                            SalvaStato();
                            return true;
                        }
                 }
            }
            SalvaStato();
            return false;
        }

        public void TryRemoveBarca(Cliente cliente, Barca barca)
        {
            /**Le altre condizioni vengono controllate internamente*/
            if (cliente == null || barca == null)
                throw new ArgumentException("Almeno uno dei parametri di invocazione è nullo!");
            cliente.RimuoviBarca(barca);
            SalvaStato();
        }

        public List<PostoBarca> FiltraPosti(Barca barca, Model.Porto porto, DateTime from, DateTime to)
        {
            if (barca == null || porto == null || from == null || to == null || from > to)
                throw new ArgumentException("Almeno uno dei parametri è non valido!");
            //Ottengo tutta la lista dei posti dal porto.
            List<PostoBarca> posti = porto.PostiBarca;
            //Creo una nuova lista dove mettere solo quelli che soddisfano i criteri del filtro
            List<PostoBarca> postiOK = new List<PostoBarca>();
            //Ciclo sui posti e faccio i controlli
            foreach (PostoBarca posto in posti)
            {
                if (posto.LunghezzaMassima < barca.Lunghezza)
                    continue;
                bool overlaps = false;
                foreach (Prenotazione prenotazione in posto.Storico.Prenotazioni)
                    if (prenotazione.Overlaps(from, to))
                        overlaps = true;
                if (overlaps)
                    continue;
                postiOK.Add(posto);
            }
            return postiOK;
        }

        public void AggiungiBarca(Cliente cliente, Barca toAdd)
        {
            /**Le altre condizioni vengono controllate internamente*/
            if (cliente == null || toAdd == null)
                throw new ArgumentException("Almeno uno dei parametri di invocazione è nullo!");
            cliente.AggiungiBarca(toAdd);
            SalvaStato();
        }

        public void Segnala(Cliente cliente, Model.Porto porto, string descrizione)
        {
            if (cliente == null || porto == null || String.IsNullOrEmpty(descrizione))
                throw new ArgumentException("Almeno uno dei parametri di invocazione è nullo o non valido!");
            Segnalazione segnalazione = new Segnalazione(cliente, descrizione);
            foreach (Model.Porto port in _gestorePorti.PortiGestiti)
                if (port.Nome == porto.Nome)
                {
                  port.Segnala(segnalazione);
                }
            foreach (Dipendente dipendenteMemory in _gestoreUtenti.Dipendenti)
                if (dipendenteMemory.LavoraPresso.Nome == porto.Nome)
                {
                    /**Lo aggiungo una sola volta all'utente, non serve che lo faccia più volte*/
                    dipendenteMemory.LavoraPresso.Segnalazioni.Add(segnalazione);
                    break;
                }

            SalvaStato();
        }

        public bool Prenota(PostoBarca postoBarca,Barca barca, DateTime from,DateTime to)
        {
            if(postoBarca==null || barca == null || from == null || to == null || from > to)
                throw new ArgumentException("Almeno uno dei parametri di invocazione è nullo o non valido!");
            DateTime newFrom = new DateTime(from.Year, from.Month, from.Day, 0, 0, 0);
            DateTime newTo = new DateTime(to.Year, to.Month, to.Day, 23, 59, 59);
            bool result = postoBarca.Prenota(barca,newFrom,newTo);
            SalvaStato();
            return result;
        }


        public IGestorePorti GestorePorti
        {
            get
            {
                return _gestorePorti;
            }
        }

        public IGestoreUtenti GestoreUtenti
        {
            get
            {
                return _gestoreUtenti;
            }
        }


        private void SalvaStato()
        {
            _gestorePorti.SavePorti();
            _gestoreUtenti.SaveUtenti();
        }
    }

}
