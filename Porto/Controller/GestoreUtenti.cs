﻿using System;
using System.Collections.Generic;
using Porto.Persistence.Interfacce;
using Porto.Persistence;
using Porto.Model.Utenti;
using Porto.Controller.Interfacce;

namespace Porto.Controller
{
    public class GestoreUtenti:IGestoreUtenti
    {
        private static readonly String fileName = "utenti.dat";
        private readonly GestorePersistenzaUtente _persistenza;
        private List<Utente> _utentiGestiti;
        private static GestoreUtenti _actualGestore = null;

        public List<Utente> UtentiGestiti
        {
            get
            {
                return _utentiGestiti;
            }
        }

        public IEnumerable<Dipendente> Dipendenti
        {
            get
            {
               List<Dipendente> dipendentiOut = new List<Dipendente>();
                foreach(Utente u in UtentiGestiti)
                {
                    if (u is Dipendente)
                        dipendentiOut.Add((Dipendente)u);
                }
                return dipendentiOut;
            }
        }

        public IEnumerable<DipendenteAmministrativo> Amministratori
        {
            get
            {
                List<DipendenteAmministrativo> dipendentiOut = new List<DipendenteAmministrativo>();
                foreach (Utente u in UtentiGestiti)
                {
                    if (u is DipendenteAmministrativo)
                        dipendentiOut.Add((DipendenteAmministrativo)u);
                }
                return dipendentiOut;
            }
        }

        private GestoreUtenti()
        {
            _persistenza = GestoriFactory.GetGestorePersistenzaUtenti(PersistenceType.Binary);
            LoadUtenti();
            SaveUtenti();
        
        }

        public static GestoreUtenti GetInstance()
        {
            if (_actualGestore == null) _actualGestore = new GestoreUtenti();
            return _actualGestore;
        }

        private void LoadUtenti()
        {
            _utentiGestiti = _persistenza.LoadUtenti(fileName);
        }

        public void SaveUtenti()
        {
            _persistenza.SaveUtenti(_utentiGestiti, fileName);
        }

        
    }
}
