﻿using Porto.Model;
using Porto.Model.Utenti;
using System;
using System.Collections.Generic;

namespace Porto.Controller.Interfacce
{
    public interface DipendenteAmministrativoController : BasicController
    {
        bool ModificaAttività(string adminCF, Attività a, string newDescrizione, List<AssistenteDiTerra> newOperators);
        bool ConfermaPrenotazione(Prenotazione daConfermare);
        bool EliminaSegnalazione(Model.Porto porto, Segnalazione segnalazione);
        bool CreaAttività(String adminCF, string descrizione, Segnalazione s, List<AssistenteDiTerra> assistenti);
        bool RimuoviAttività(String adminCF, Attività attività);
    }
}
