﻿using Porto.Model.Utenti;
using System.Collections.Generic;

namespace Porto.Controller.Interfacce
{
    public interface IGestoreUtenti
    {
        List<Utente> UtentiGestiti { get; }
        IEnumerable<Dipendente> Dipendenti { get; }
        IEnumerable<DipendenteAmministrativo> Amministratori { get; }
        void SaveUtenti();
    }
}
