﻿using Porto.Model;
using Porto.Model.Utenti;
using System.Collections.Generic;

namespace Porto.Controller.Interfacce
{
    public interface IGestorePorti
    {
        List<Model.Porto> PortiGestiti { get; }
        void SavePorti();
        List<Prenotazione> OttieniTuttePrenotazioniPerCliente(Cliente p);
        Model.Porto OttieniPortoPerDipendente(Dipendente dip);
        IEnumerable<Model.Porto> OttieniPortiPerCliente(Cliente cliente);
    }
}
