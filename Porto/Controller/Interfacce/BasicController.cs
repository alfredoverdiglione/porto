﻿using Porto.Model.Utenti;

namespace Porto.Controller.Interfacce
{
    public interface BasicController
    {
        Utente LogIn(string username, string password);
        IGestorePorti GestorePorti { get; }
        IGestoreUtenti GestoreUtenti { get; }
    }
}
