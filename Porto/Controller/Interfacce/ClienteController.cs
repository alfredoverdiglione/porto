﻿using Porto.Model;
using Porto.Model.Utenti;
using System;
using System.Collections.Generic;

namespace Porto.Controller.Interfacce
{
    public interface ClienteController:BasicController
    {
        bool EliminaSegnalazione(Model.Porto porto, Segnalazione segnalazione);
        void ModificaBarca(Barca daModificare, String nuovaTarga, String nuovoNome);
        bool CancellaPrenotazione(Prenotazione p);
        void TryRemoveBarca(Cliente cliente, Barca barca);
        List<PostoBarca> FiltraPosti(Barca barca, Model.Porto porto, DateTime from, DateTime to);
        void AggiungiBarca(Cliente _cliente, Barca toAdd);
        void Segnala(Cliente cliente, Model.Porto porto, string descrizione);
        bool Prenota(PostoBarca postoBarca, Barca barca, DateTime from, DateTime to);

    }
}
