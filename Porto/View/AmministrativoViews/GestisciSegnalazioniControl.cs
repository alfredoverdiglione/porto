﻿using System;
using System.Windows.Forms;
using Porto.Controller.Interfacce;
using Porto.Model.Utenti;
using Porto.Model;

namespace Porto.View
{

    public delegate bool EliminaSegnalazione(Model.Porto porto,Segnalazione segnalazione);

    public partial class GestisciSegnalazioniControl : UserControl {
        public event EliminaSegnalazione EliminaSegnalazione;
        private DipendenteAmministrativo _admin;
        private DipendenteAmministrativoController _controller;

        public GestisciSegnalazioniControl() {
            InitializeComponent();
        }

        public GestisciSegnalazioniControl(DipendenteAmministrativo _admin, DipendenteAmministrativoController _controller):this()
        {
            if (_admin == null || _controller == null)
                throw new ArgumentException("Uno dei parametri di inizializzazione è nullo!");
            this._admin = _admin;
            this._controller = _controller;

            _optionalPanel.Name = "Dettagli Segnalazione";

            _controller.GestorePorti.OttieniPortoPerDipendente(_admin).SegnalazioneRemoved += SegnalazioniChanged;
            _controller.GestorePorti.OttieniPortoPerDipendente(_admin).SegnalazioneAdded += SegnalazioniChanged;
            EliminaSegnalazione += _controller.EliminaSegnalazione;


            _segnalazioniListBox.DataSource = _controller.GestorePorti.OttieniPortoPerDipendente(_admin).Segnalazioni;
            _segnalazioniListBox.SelectedIndex = -1;
            _segnalazioniListBox.SelectedIndexChanged += SelectedItemChanged;
            _selezionaButton.Enabled = false;
            _selezionaButton.Click += TrySeleziona;
            _eliminaButton.Enabled = false;
            _eliminaButton.Click += TryRemove;
            RefreshTextBoxes();


            
        }

        private void TryRemove(object sender, EventArgs e)
        {
            if(MessageBox.Show("Sei sicuro di voler rimuovere?", "Conferma", MessageBoxButtons.YesNo)
                == DialogResult.Yes)
            {
                if (_segnalazioniListBox.SelectedIndex > -1 &&
                    EliminaSegnalazione != null)
                    EliminaSegnalazione(_controller.GestorePorti.OttieniPortoPerDipendente(_admin), (Segnalazione)_segnalazioniListBox.SelectedItem);
            }
        }

        private void SegnalazioniChanged(object sender, EventArgs e)
        {
            _segnalazioniListBox.DataSource = null;
            _segnalazioniListBox.DataSource = _controller.GestorePorti.OttieniPortoPerDipendente(_admin).Segnalazioni;
            _segnalazioniListBox.SelectedIndex = -1;
            RefreshTextBoxes();
        }

        private void RefreshTextBoxes()
        {
            _autoreTextBox.Text = "Autore: ";
            _descrizioneTextBox.Text = "Descrizione:\n";
        }

        private void TrySeleziona(object sender, EventArgs e)
        {
            if (_segnalazioniListBox.SelectedIndex > -1)
            {
                _eliminaButton.Enabled = true;
                Segnalazione segnalazione = (Segnalazione)(_segnalazioniListBox.SelectedItem);
                _autoreTextBox.Text = "Autore: "+segnalazione.Autore.Nome + " "+segnalazione.Autore.Cognome;
                _descrizioneTextBox.Text = "Descrizione:\n"+segnalazione.Descrizione;
            }

        }

        private void SelectedItemChanged(object sender, EventArgs e)
        {
            if (_segnalazioniListBox.SelectedIndex > -1)
            {
                _selezionaButton.Enabled = true;
                _eliminaButton.Enabled = false;
                RefreshTextBoxes();
            }
        }
    }
}
