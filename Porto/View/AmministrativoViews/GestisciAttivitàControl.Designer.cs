﻿namespace Porto.View.AmministrativoViews{
    partial class GestisciAttivitàControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label3 = new System.Windows.Forms.Label();
            this._attivitàListBox = new System.Windows.Forms.ListBox();
            this._creaAttivitàButton = new System.Windows.Forms.Button();
            this._selezionaButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this._eliminaButton = new System.Windows.Forms.Button();
            this._modificaButton = new System.Windows.Forms.Button();
            this._segnalazioneAuthorText = new System.Windows.Forms.TextBox();
            this._segnalazioneDescriptionText = new System.Windows.Forms.TextBox();
            this._descrizioneTextBox = new System.Windows.Forms.TextBox();
            this._operatoriListBox = new System.Windows.Forms.ListBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(2, 6);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(179, 26);
            this.label3.TabIndex = 8;
            this.label3.Text = "Lista delle attività";
            // 
            // _attivitàListBox
            // 
            this._attivitàListBox.FormattingEnabled = true;
            this._attivitàListBox.Location = new System.Drawing.Point(2, 36);
            this._attivitàListBox.Margin = new System.Windows.Forms.Padding(2);
            this._attivitàListBox.Name = "_attivitàListBox";
            this._attivitàListBox.Size = new System.Drawing.Size(322, 82);
            this._attivitàListBox.TabIndex = 9;
            // 
            // _creaAttivitàButton
            // 
            this._creaAttivitàButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._creaAttivitàButton.Location = new System.Drawing.Point(328, 2);
            this._creaAttivitàButton.Margin = new System.Windows.Forms.Padding(2);
            this._creaAttivitàButton.Name = "_creaAttivitàButton";
            this._creaAttivitàButton.Size = new System.Drawing.Size(120, 30);
            this._creaAttivitàButton.TabIndex = 10;
            this._creaAttivitàButton.Text = "Crea Nuova Attività";
            this._creaAttivitàButton.UseVisualStyleBackColor = true;
            // 
            // _selezionaButton
            // 
            this._selezionaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._selezionaButton.Location = new System.Drawing.Point(328, 36);
            this._selezionaButton.Margin = new System.Windows.Forms.Padding(2);
            this._selezionaButton.Name = "_selezionaButton";
            this._selezionaButton.Size = new System.Drawing.Size(120, 32);
            this._selezionaButton.TabIndex = 20;
            this._selezionaButton.Text = "Seleziona";
            this._selezionaButton.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this._eliminaButton);
            this.panel1.Controls.Add(this._modificaButton);
            this.panel1.Controls.Add(this._segnalazioneAuthorText);
            this.panel1.Controls.Add(this._segnalazioneDescriptionText);
            this.panel1.Controls.Add(this._descrizioneTextBox);
            this.panel1.Controls.Add(this._operatoriListBox);
            this.panel1.Location = new System.Drawing.Point(4, 124);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(443, 361);
            this.panel1.TabIndex = 21;
            // 
            // _eliminaButton
            // 
            this._eliminaButton.Enabled = false;
            this._eliminaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._eliminaButton.Location = new System.Drawing.Point(320, 38);
            this._eliminaButton.Margin = new System.Windows.Forms.Padding(2);
            this._eliminaButton.Name = "_eliminaButton";
            this._eliminaButton.Size = new System.Drawing.Size(120, 32);
            this._eliminaButton.TabIndex = 23;
            this._eliminaButton.Text = "Elimina";
            this._eliminaButton.UseVisualStyleBackColor = true;
            // 
            // _modificaButton
            // 
            this._modificaButton.Enabled = false;
            this._modificaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._modificaButton.Location = new System.Drawing.Point(320, 2);
            this._modificaButton.Margin = new System.Windows.Forms.Padding(2);
            this._modificaButton.Name = "_modificaButton";
            this._modificaButton.Size = new System.Drawing.Size(120, 32);
            this._modificaButton.TabIndex = 22;
            this._modificaButton.Text = "Modifica";
            this._modificaButton.UseVisualStyleBackColor = true;
            // 
            // _segnalazioneAuthorText
            // 
            this._segnalazioneAuthorText.Location = new System.Drawing.Point(246, 130);
            this._segnalazioneAuthorText.Multiline = true;
            this._segnalazioneAuthorText.Name = "_segnalazioneAuthorText";
            this._segnalazioneAuthorText.ReadOnly = true;
            this._segnalazioneAuthorText.Size = new System.Drawing.Size(194, 31);
            this._segnalazioneAuthorText.TabIndex = 3;
            // 
            // _segnalazioneDescriptionText
            // 
            this._segnalazioneDescriptionText.Location = new System.Drawing.Point(246, 167);
            this._segnalazioneDescriptionText.Multiline = true;
            this._segnalazioneDescriptionText.Name = "_segnalazioneDescriptionText";
            this._segnalazioneDescriptionText.ReadOnly = true;
            this._segnalazioneDescriptionText.Size = new System.Drawing.Size(194, 191);
            this._segnalazioneDescriptionText.TabIndex = 2;
            // 
            // _descrizioneTextBox
            // 
            this._descrizioneTextBox.Location = new System.Drawing.Point(4, 130);
            this._descrizioneTextBox.Multiline = true;
            this._descrizioneTextBox.Name = "_descrizioneTextBox";
            this._descrizioneTextBox.ReadOnly = true;
            this._descrizioneTextBox.Size = new System.Drawing.Size(236, 228);
            this._descrizioneTextBox.TabIndex = 1;
            // 
            // _operatoriListBox
            // 
            this._operatoriListBox.Enabled = false;
            this._operatoriListBox.FormattingEnabled = true;
            this._operatoriListBox.Location = new System.Drawing.Point(3, 3);
            this._operatoriListBox.Name = "_operatoriListBox";
            this._operatoriListBox.Size = new System.Drawing.Size(312, 121);
            this._operatoriListBox.TabIndex = 0;
            // 
            // GestisciAttivitàControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this._selezionaButton);
            this.Controls.Add(this._creaAttivitàButton);
            this.Controls.Add(this._attivitàListBox);
            this.Controls.Add(this.label3);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "GestisciAttivitàControl";
            this.Size = new System.Drawing.Size(450, 488);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox _attivitàListBox;
        private System.Windows.Forms.Button _creaAttivitàButton;
        private System.Windows.Forms.Button _selezionaButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListBox _operatoriListBox;
        private System.Windows.Forms.TextBox _descrizioneTextBox;
        private System.Windows.Forms.TextBox _segnalazioneDescriptionText;
        private System.Windows.Forms.TextBox _segnalazioneAuthorText;
        private System.Windows.Forms.Button _eliminaButton;
        private System.Windows.Forms.Button _modificaButton;
    }
}
