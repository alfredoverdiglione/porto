﻿using System;
using System.Windows.Forms;
using Porto.Controller.Interfacce;
using Porto.Model;

namespace Porto.View
{
    public delegate Boolean ConfermaPrenotazione(Prenotazione daConfermare);

    public partial class GestisciPrenotazioniAmministratoreControl : UserControl {
        public event ConfermaPrenotazione ConfermaPrenotazione;
        private DipendenteAmministrativoController _controller;
        private Model.Porto _portoAmministrato;

        public GestisciPrenotazioniAmministratoreControl() {
            InitializeComponent();
        }

        public GestisciPrenotazioniAmministratoreControl(Model.Porto _portoAmministrato, DipendenteAmministrativoController _controller):this()
        {
            if (_portoAmministrato == null || _controller == null)
                throw new ArgumentException("Almeno uno dei parametri è nullo!");

            this._portoAmministrato = _portoAmministrato;
            this._controller = _controller;

            /**Mi lego agli eventi del modello*/
            foreach(PostoBarca pb in _portoAmministrato.PostiBarca)
            {
                pb.PrenotazioneAdded += OnPrenotazioneAdded;
                pb.PrenotazioneRemoved += OnPrenotazioniRemoved;
            }

            ConfermaPrenotazione += _controller.ConfermaPrenotazione;

            _prenotazioniListBox.DataSource = _portoAmministrato.TuttePrenotazioni;
            _prenotazioniListBox.SelectedIndex = -1;
            _selezionaButton.Click += OnTrySeleziona;
            _confermaButton.Click += OnConferma;
            RefreshTextBox();

        }

        private void RefreshTextBox()
        {
            _aText.Text = "A: ";
            _daText.Text = "Da: ";
            _barcaText.Text = "Barca: ";
            _idText.Text = "Id: ";
            _postoBarcaText.Text = "Posto Barca: ";
            _prezzoText.Text = "Prezzo: ";
            _statoPrenotazioneText.Text = "Stato Prenotazione: ";
        }

        private void OnPrenotazioniRemoved(object sender, EventArgs e)
        {
            RefreshPrenotazioneList();
        }

        private void OnPrenotazioneAdded(object sender, EventArgs e)
        {
            RefreshPrenotazioneList();
        }

        private void OnConferma(object sender, EventArgs e)
        {
            if (_prenotazioniListBox.SelectedIndex > -1)
            {
                Prenotazione selezionata = (Prenotazione)_prenotazioniListBox.SelectedItem;
                if (ConfermaPrenotazione != null)
                    if (ConfermaPrenotazione(selezionata))
                        MessageBox.Show("Conferma andata a buon fine!");
                RefreshPrenotazioneList();
                RefreshTextBox();
            }
        }

        private void RefreshPrenotazioneList()
        {
            _prenotazioniListBox.DataSource = null;
            _prenotazioniListBox.DataSource = _portoAmministrato.TuttePrenotazioni;
            _prenotazioniListBox.SelectedIndex = -1;
            _confermaButton.Enabled = false;
        }

        private void OnTrySeleziona(object sender, EventArgs e)
        {
            if (_prenotazioniListBox.SelectedIndex > -1)
            {
                Prenotazione selezionata = (Prenotazione)_prenotazioniListBox.SelectedItem;
                if (selezionata.StatoPrenotazione == StatoPrenotazione.NonConfermata)
                _confermaButton.Enabled = true;

                _aText.Text = "A: " + selezionata.DataFine;
                _daText.Text = "Da: " + selezionata.DataInizio;
                _barcaText.Text = "Barca: " + selezionata.Barca;
                _idText.Text = "Id: " + selezionata.Id;
                _postoBarcaText.Text = "Posto Barca: " + selezionata.PostoBarca.Id;
                _statoPrenotazioneText.Text = "Stato Prenotazione: " + selezionata.StatoPrenotazione;
                _prezzoText.Text = "Prezzo: "+selezionata.Prezzo;


            }
        }

        
    }
}
