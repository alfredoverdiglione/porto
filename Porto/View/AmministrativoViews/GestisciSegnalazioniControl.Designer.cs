﻿namespace Porto.View {
    partial class GestisciSegnalazioniControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label3 = new System.Windows.Forms.Label();
            this._segnalazioniListBox = new System.Windows.Forms.ListBox();
            this._selezionaButton = new System.Windows.Forms.Button();
            this._optionalPanel = new System.Windows.Forms.Panel();
            this._descrizioneTextBox = new System.Windows.Forms.TextBox();
            this._autoreTextBox = new System.Windows.Forms.TextBox();
            this._eliminaButton = new System.Windows.Forms.Button();
            this._optionalPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(2, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(237, 26);
            this.label3.TabIndex = 8;
            this.label3.Text = "Lista delle segnalazioni";
            // 
            // _segnalazioniListBox
            // 
            this._segnalazioniListBox.FormattingEnabled = true;
            this._segnalazioniListBox.Location = new System.Drawing.Point(7, 67);
            this._segnalazioniListBox.Margin = new System.Windows.Forms.Padding(2);
            this._segnalazioniListBox.Name = "_segnalazioniListBox";
            this._segnalazioniListBox.Size = new System.Drawing.Size(434, 82);
            this._segnalazioniListBox.TabIndex = 9;
            // 
            // _selezionaButton
            // 
            this._selezionaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._selezionaButton.Location = new System.Drawing.Point(7, 32);
            this._selezionaButton.Margin = new System.Windows.Forms.Padding(2);
            this._selezionaButton.Name = "_selezionaButton";
            this._selezionaButton.Size = new System.Drawing.Size(109, 31);
            this._selezionaButton.TabIndex = 20;
            this._selezionaButton.Text = "Seleziona";
            this._selezionaButton.UseVisualStyleBackColor = true;
            // 
            // _optionalPanel
            // 
            this._optionalPanel.Controls.Add(this._descrizioneTextBox);
            this._optionalPanel.Controls.Add(this._autoreTextBox);
            this._optionalPanel.Controls.Add(this._eliminaButton);
            this._optionalPanel.Location = new System.Drawing.Point(7, 155);
            this._optionalPanel.Name = "_optionalPanel";
            this._optionalPanel.Size = new System.Drawing.Size(434, 330);
            this._optionalPanel.TabIndex = 21;
            // 
            // _descrizioneTextBox
            // 
            this._descrizioneTextBox.Location = new System.Drawing.Point(4, 30);
            this._descrizioneTextBox.Multiline = true;
            this._descrizioneTextBox.Name = "_descrizioneTextBox";
            this._descrizioneTextBox.ReadOnly = true;
            this._descrizioneTextBox.Size = new System.Drawing.Size(427, 297);
            this._descrizioneTextBox.TabIndex = 2;
            // 
            // _autoreTextBox
            // 
            this._autoreTextBox.Location = new System.Drawing.Point(4, 4);
            this._autoreTextBox.Name = "_autoreTextBox";
            this._autoreTextBox.ReadOnly = true;
            this._autoreTextBox.Size = new System.Drawing.Size(346, 20);
            this._autoreTextBox.TabIndex = 1;
            // 
            // _eliminaButton
            // 
            this._eliminaButton.Location = new System.Drawing.Point(356, 4);
            this._eliminaButton.Name = "_eliminaButton";
            this._eliminaButton.Size = new System.Drawing.Size(75, 23);
            this._eliminaButton.TabIndex = 0;
            this._eliminaButton.Text = "Elimina";
            this._eliminaButton.UseVisualStyleBackColor = true;
            // 
            // GestisciSegnalazioniControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this._optionalPanel);
            this.Controls.Add(this._selezionaButton);
            this.Controls.Add(this._segnalazioniListBox);
            this.Controls.Add(this.label3);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "GestisciSegnalazioniControl";
            this.Size = new System.Drawing.Size(450, 488);
            this._optionalPanel.ResumeLayout(false);
            this._optionalPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox _segnalazioniListBox;
        private System.Windows.Forms.Button _selezionaButton;
        private System.Windows.Forms.Panel _optionalPanel;
        private System.Windows.Forms.Button _eliminaButton;
        private System.Windows.Forms.TextBox _autoreTextBox;
        private System.Windows.Forms.TextBox _descrizioneTextBox;
    }
}
