﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Porto.Controller.Interfacce;
using Porto.Model;
using Porto.Model.Utenti;
using Porto.Model.Interfacce;

namespace Porto.View.AmministrativoViews
{
    public delegate Boolean ModifyAttività(String adminCF, Attività a, String newDescrizione, List<AssistenteDiTerra> newOperators);

    public partial class ModificaAttivitàControl : UserControl
    {
        public event Close Close;
        public event ModifyAttività ModifyAttività;

        private DipendenteAmministrativo _admin;
        private Attività _attività;
        private Model.Porto _porto;
        private DipendenteAmministrativoController _controller;

        public ModificaAttivitàControl()
        {
            InitializeComponent();
        }

        public ModificaAttivitàControl(DipendenteAmministrativo a, Attività descrizione, Model.Porto porto, DipendenteAmministrativoController _controller):this()
        {
            if (a == null || descrizione == null || porto == null || _controller == null)
                throw new ArgumentException("Uno dei parametri di inizializzazione è nullo!");

            _attività = descrizione;
            _porto = porto;
            this._admin = a;
            this._controller = _controller;
            ModifyAttività += _controller.ModificaAttività;
            _descrizioneTextBox.Text = _attività.Descrizione;
            _operatoriListBox.DataSource = porto.AssistentiDiTerra;
            _operatoriListBox.DisplayMember = "NomeCognome";
            _operatoriListBox.SelectedIndex = -1;
            _descrizioneTextBox.KeyUp += CheckValidity;
            _modificaOperatoriButton.Click += AllowModify;

            _cancelButton.Click += Quit;
            _okButton.Click += TryModify;
        }

        private void CheckValidity(object sender, KeyEventArgs e)
        {
            _okButton.Enabled = _descrizioneTextBox.Text.Length > 0 ? true : false;
        }

        private void TryModify(object sender, EventArgs e)
        {
            if (ModifyAttività != null)
            {
                List<AssistenteDiTerra> assistentiNuovi = new List<AssistenteDiTerra>();
                foreach(AssistenteDiTerra ast in _operatoriListBox.SelectedItems.Cast<AssistenteDiTerra>())
                {
                    assistentiNuovi.Add(ast);
                }

                if (!_operatoriListBox.Enabled)
                {
                    foreach(AssistenteDiTerra ast in _controller.GestorePorti.OttieniPortoPerDipendente(_admin).AssistentiDiTerra)
                        foreach(IAssistenteDiTerra iast in _attività.Operatori)
                    {
                            if (iast.CodiceFiscale == ast.CodiceFiscale)
                                assistentiNuovi.Add(ast);

                    }
                }
               if( ModifyAttività(_admin.CodiceFiscale, _attività, _descrizioneTextBox.Text,assistentiNuovi))
                    MessageBox.Show("Modifica riuscita!");
            }

            if (Close != null)
                Close();
        }

        private void Quit(object sender, EventArgs e)
        {
            if (Close != null)
                Close();
        }

        private void AllowModify(object sender, EventArgs e)
        {
            _operatoriListBox.Enabled = true;
        }
    }
}
