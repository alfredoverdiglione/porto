﻿namespace Porto.View {
    partial class GestisciPrenotazioniAmministratoreControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label3 = new System.Windows.Forms.Label();
            this._prenotazioniListBox = new System.Windows.Forms.ListBox();
            this._selezionaButton = new System.Windows.Forms.Button();
            this._detailsPanel = new System.Windows.Forms.Panel();
            this._statoPrenotazioneText = new System.Windows.Forms.TextBox();
            this._prezzoText = new System.Windows.Forms.TextBox();
            this._postoBarcaText = new System.Windows.Forms.TextBox();
            this._idText = new System.Windows.Forms.TextBox();
            this._barcaText = new System.Windows.Forms.TextBox();
            this._aText = new System.Windows.Forms.TextBox();
            this._daText = new System.Windows.Forms.TextBox();
            this._confermaButton = new System.Windows.Forms.Button();
            this._detailsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(2, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(234, 26);
            this.label3.TabIndex = 7;
            this.label3.Text = "Lista delle prenotazioni";
            // 
            // _prenotazioniListBox
            // 
            this._prenotazioniListBox.FormattingEnabled = true;
            this._prenotazioniListBox.Location = new System.Drawing.Point(2, 67);
            this._prenotazioniListBox.Margin = new System.Windows.Forms.Padding(2);
            this._prenotazioniListBox.Name = "_prenotazioniListBox";
            this._prenotazioniListBox.Size = new System.Drawing.Size(446, 121);
            this._prenotazioniListBox.TabIndex = 8;
            // 
            // _selezionaButton
            // 
            this._selezionaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._selezionaButton.Location = new System.Drawing.Point(242, 28);
            this._selezionaButton.Margin = new System.Windows.Forms.Padding(2);
            this._selezionaButton.Name = "_selezionaButton";
            this._selezionaButton.Size = new System.Drawing.Size(206, 35);
            this._selezionaButton.TabIndex = 19;
            this._selezionaButton.Text = "Seleziona";
            this._selezionaButton.UseVisualStyleBackColor = true;
            // 
            // _detailsPanel
            // 
            this._detailsPanel.Controls.Add(this._statoPrenotazioneText);
            this._detailsPanel.Controls.Add(this._prezzoText);
            this._detailsPanel.Controls.Add(this._postoBarcaText);
            this._detailsPanel.Controls.Add(this._idText);
            this._detailsPanel.Controls.Add(this._barcaText);
            this._detailsPanel.Controls.Add(this._aText);
            this._detailsPanel.Controls.Add(this._daText);
            this._detailsPanel.Controls.Add(this._confermaButton);
            this._detailsPanel.Location = new System.Drawing.Point(4, 194);
            this._detailsPanel.Name = "_detailsPanel";
            this._detailsPanel.Size = new System.Drawing.Size(443, 291);
            this._detailsPanel.TabIndex = 20;
            // 
            // _statoPrenotazioneText
            // 
            this._statoPrenotazioneText.Location = new System.Drawing.Point(3, 109);
            this._statoPrenotazioneText.Name = "_statoPrenotazioneText";
            this._statoPrenotazioneText.ReadOnly = true;
            this._statoPrenotazioneText.Size = new System.Drawing.Size(436, 20);
            this._statoPrenotazioneText.TabIndex = 7;
            // 
            // _prezzoText
            // 
            this._prezzoText.Location = new System.Drawing.Point(3, 83);
            this._prezzoText.Name = "_prezzoText";
            this._prezzoText.ReadOnly = true;
            this._prezzoText.Size = new System.Drawing.Size(436, 20);
            this._prezzoText.TabIndex = 6;
            // 
            // _postoBarcaText
            // 
            this._postoBarcaText.Location = new System.Drawing.Point(3, 57);
            this._postoBarcaText.Name = "_postoBarcaText";
            this._postoBarcaText.ReadOnly = true;
            this._postoBarcaText.Size = new System.Drawing.Size(436, 20);
            this._postoBarcaText.TabIndex = 5;
            // 
            // _idText
            // 
            this._idText.Location = new System.Drawing.Point(3, 6);
            this._idText.Name = "_idText";
            this._idText.ReadOnly = true;
            this._idText.Size = new System.Drawing.Size(127, 20);
            this._idText.TabIndex = 4;
            // 
            // _barcaText
            // 
            this._barcaText.Location = new System.Drawing.Point(4, 31);
            this._barcaText.Name = "_barcaText";
            this._barcaText.ReadOnly = true;
            this._barcaText.Size = new System.Drawing.Size(436, 20);
            this._barcaText.TabIndex = 3;
            // 
            // _aText
            // 
            this._aText.Location = new System.Drawing.Point(259, 6);
            this._aText.Name = "_aText";
            this._aText.ReadOnly = true;
            this._aText.Size = new System.Drawing.Size(100, 20);
            this._aText.TabIndex = 2;
            // 
            // _daText
            // 
            this._daText.Location = new System.Drawing.Point(136, 6);
            this._daText.Name = "_daText";
            this._daText.ReadOnly = true;
            this._daText.Size = new System.Drawing.Size(117, 20);
            this._daText.TabIndex = 1;
            // 
            // _confermaButton
            // 
            this._confermaButton.Enabled = false;
            this._confermaButton.Location = new System.Drawing.Point(365, 3);
            this._confermaButton.Name = "_confermaButton";
            this._confermaButton.Size = new System.Drawing.Size(75, 23);
            this._confermaButton.TabIndex = 0;
            this._confermaButton.Text = "Conferma";
            this._confermaButton.UseVisualStyleBackColor = true;
            // 
            // GestisciPrenotazioniAmministratoreControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this._detailsPanel);
            this.Controls.Add(this._selezionaButton);
            this.Controls.Add(this._prenotazioniListBox);
            this.Controls.Add(this.label3);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "GestisciPrenotazioniAmministratoreControl";
            this.Size = new System.Drawing.Size(450, 488);
            this._detailsPanel.ResumeLayout(false);
            this._detailsPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox _prenotazioniListBox;
        private System.Windows.Forms.Button _selezionaButton;
        private System.Windows.Forms.Panel _detailsPanel;
        private System.Windows.Forms.Button _confermaButton;
        private System.Windows.Forms.TextBox _daText;
        private System.Windows.Forms.TextBox _statoPrenotazioneText;
        private System.Windows.Forms.TextBox _prezzoText;
        private System.Windows.Forms.TextBox _postoBarcaText;
        private System.Windows.Forms.TextBox _idText;
        private System.Windows.Forms.TextBox _barcaText;
        private System.Windows.Forms.TextBox _aText;
    }
}
