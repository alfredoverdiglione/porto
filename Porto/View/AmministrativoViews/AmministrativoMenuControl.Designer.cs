﻿namespace Porto.View.AmministrativoViews {
    partial class AmministrativoMenuControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this._gestisciSegnalazioniButton = new System.Windows.Forms.Button();
            this._gestisciPrenotazioniButton = new System.Windows.Forms.Button();
            this._gestisciAttivitàButton = new System.Windows.Forms.Button();
            this._logOutButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _gestisciSegnalazioniButton
            // 
            this._gestisciSegnalazioniButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._gestisciSegnalazioniButton.Location = new System.Drawing.Point(2, 2);
            this._gestisciSegnalazioniButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._gestisciSegnalazioniButton.Name = "_gestisciSegnalazioniButton";
            this._gestisciSegnalazioniButton.Size = new System.Drawing.Size(146, 57);
            this._gestisciSegnalazioniButton.TabIndex = 0;
            this._gestisciSegnalazioniButton.Text = "Gestisci Segnalazioni";
            this._gestisciSegnalazioniButton.UseVisualStyleBackColor = true;
            // 
            // _gestisciPrenotazioniButton
            // 
            this._gestisciPrenotazioniButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._gestisciPrenotazioniButton.Location = new System.Drawing.Point(2, 64);
            this._gestisciPrenotazioniButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._gestisciPrenotazioniButton.Name = "_gestisciPrenotazioniButton";
            this._gestisciPrenotazioniButton.Size = new System.Drawing.Size(146, 57);
            this._gestisciPrenotazioniButton.TabIndex = 1;
            this._gestisciPrenotazioniButton.Text = "Gestisci Prenotazioni";
            this._gestisciPrenotazioniButton.UseVisualStyleBackColor = true;
            // 
            // _gestisciAttivitàButton
            // 
            this._gestisciAttivitàButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._gestisciAttivitàButton.Location = new System.Drawing.Point(2, 126);
            this._gestisciAttivitàButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._gestisciAttivitàButton.Name = "_gestisciAttivitàButton";
            this._gestisciAttivitàButton.Size = new System.Drawing.Size(146, 57);
            this._gestisciAttivitàButton.TabIndex = 2;
            this._gestisciAttivitàButton.Text = "GestisciAttività";
            this._gestisciAttivitàButton.UseVisualStyleBackColor = true;
            // 
            // _logOutButton
            // 
            this._logOutButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._logOutButton.Location = new System.Drawing.Point(2, 428);
            this._logOutButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._logOutButton.Name = "_logOutButton";
            this._logOutButton.Size = new System.Drawing.Size(146, 57);
            this._logOutButton.TabIndex = 4;
            this._logOutButton.Text = "Log Out";
            this._logOutButton.UseVisualStyleBackColor = true;
            // 
            // AmministrativoMenuControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this._logOutButton);
            this.Controls.Add(this._gestisciAttivitàButton);
            this.Controls.Add(this._gestisciPrenotazioniButton);
            this.Controls.Add(this._gestisciSegnalazioniButton);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "AmministrativoMenuControl";
            this.Size = new System.Drawing.Size(150, 488);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _gestisciSegnalazioniButton;
        private System.Windows.Forms.Button _gestisciPrenotazioniButton;
        private System.Windows.Forms.Button _gestisciAttivitàButton;
        private System.Windows.Forms.Button _logOutButton;
    }
}
