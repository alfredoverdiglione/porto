﻿namespace Porto.View.AmministrativoViews
{
    partial class ModificaAttivitàControl
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this._descrizioneTextBox = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this._cancelButton = new System.Windows.Forms.Button();
            this._okButton = new System.Windows.Forms.Button();
            this._operatoriListBox = new System.Windows.Forms.ListBox();
            this._modificaOperatoriButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _descrizioneTextBox
            // 
            this._descrizioneTextBox.Location = new System.Drawing.Point(3, 19);
            this._descrizioneTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._descrizioneTextBox.Multiline = true;
            this._descrizioneTextBox.Name = "_descrizioneTextBox";
            this._descrizioneTextBox.Size = new System.Drawing.Size(296, 49);
            this._descrizioneTextBox.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this._cancelButton);
            this.panel1.Controls.Add(this._okButton);
            this.panel1.Controls.Add(this._operatoriListBox);
            this.panel1.Controls.Add(this._modificaOperatoriButton);
            this.panel1.Location = new System.Drawing.Point(3, 72);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(295, 169);
            this.panel1.TabIndex = 1;
            // 
            // _cancelButton
            // 
            this._cancelButton.Location = new System.Drawing.Point(236, 144);
            this._cancelButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._cancelButton.Name = "_cancelButton";
            this._cancelButton.Size = new System.Drawing.Size(56, 19);
            this._cancelButton.TabIndex = 3;
            this._cancelButton.Text = "Cancel";
            this._cancelButton.UseVisualStyleBackColor = true;
            // 
            // _okButton
            // 
            this._okButton.Location = new System.Drawing.Point(236, 124);
            this._okButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._okButton.Name = "_okButton";
            this._okButton.Size = new System.Drawing.Size(56, 19);
            this._okButton.TabIndex = 2;
            this._okButton.Text = "Ok";
            this._okButton.UseVisualStyleBackColor = true;
            // 
            // _operatoriListBox
            // 
            this._operatoriListBox.Enabled = false;
            this._operatoriListBox.FormattingEnabled = true;
            this._operatoriListBox.Location = new System.Drawing.Point(2, 3);
            this._operatoriListBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._operatoriListBox.Name = "_operatoriListBox";
            this._operatoriListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this._operatoriListBox.Size = new System.Drawing.Size(204, 160);
            this._operatoriListBox.TabIndex = 1;
            // 
            // _modificaOperatoriButton
            // 
            this._modificaOperatoriButton.Location = new System.Drawing.Point(210, 3);
            this._modificaOperatoriButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._modificaOperatoriButton.Name = "_modificaOperatoriButton";
            this._modificaOperatoriButton.Size = new System.Drawing.Size(82, 19);
            this._modificaOperatoriButton.TabIndex = 0;
            this._modificaOperatoriButton.Text = "Modifica Operatori";
            this._modificaOperatoriButton.UseVisualStyleBackColor = true;
            // 
            // ModificaAttivitàControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this._descrizioneTextBox);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "ModificaAttivitàControl";
            this.Size = new System.Drawing.Size(300, 244);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _descrizioneTextBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button _modificaOperatoriButton;
        private System.Windows.Forms.ListBox _operatoriListBox;
        private System.Windows.Forms.Button _okButton;
        private System.Windows.Forms.Button _cancelButton;
    }
}
