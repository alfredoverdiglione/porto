﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Porto.Model;
using Porto.Model.Utenti;
using Porto.Controller.Interfacce;

namespace Porto.View.AmministrativoViews
{
    public delegate void Close();
    public delegate bool CreaAttività(String adminCF, String descrizione, Segnalazione s,List<AssistenteDiTerra> assistenti);
    public partial class CreaAttivitàControl : UserControl
    {
        public event Close Close;
        public event CreaAttività CreaAttività;
        private List<Segnalazione> segnalazioni;
        private DipendenteAmministrativoController _controller;
        private DipendenteAmministrativo _admin;

        public CreaAttivitàControl()
        {
            InitializeComponent();
        }

        public CreaAttivitàControl(List<Segnalazione> segnalazioni,DipendenteAmministrativo admin, DipendenteAmministrativoController controller):this()
        {
            if (segnalazioni == null || admin == null || controller == null)
                throw new ArgumentException("Almeno uno dei parametri di inizializzazione è nullo!");

            this.segnalazioni = segnalazioni;
            _controller = controller;
            _admin = admin;
            _confermaButton.Enabled = false;

            _segnalazioneListBox.DataSource = segnalazioni;
            _segnalazioneListBox.SelectedIndex = -1;

            _operatoriListBox.DataSource = _controller.GestorePorti.OttieniPortoPerDipendente(_admin).AssistentiDiTerra;

            
            _operatoriListBox.DisplayMember = "NomeCognome";
            _operatoriListBox.SelectedIndex = -1;
            _segnalazioneListBox.SelectedIndexChanged += CheckStatus;
            _operatoriListBox.SelectedIndexChanged += CheckStatus;
            _descrizioneText.KeyUp += CheckStatus;
            CreaAttività += _controller.CreaAttività;
            _confermaButton.Click += TryConferma ;
            _confermaButton.Click += OnClose;
            _cancelButton.Click += OnClose;

            Dock = DockStyle.Fill;
        }

        private void TryConferma(object sender, EventArgs e)
        {
            if (CreaAttività != null)
            {
                List<AssistenteDiTerra> operatori = new List<AssistenteDiTerra>(_operatoriListBox.SelectedItems.Cast<AssistenteDiTerra>());
                CreaAttività(_admin.CodiceFiscale, _descrizioneText.Text, (Segnalazione)_segnalazioneListBox.SelectedItem, operatori);
            }
        }

        private void OnClose(object sender, EventArgs e)
        {
            if (Close != null)
                Close();
        }

        private void CheckStatus(object sender, EventArgs e)
        {
            if (_segnalazioneListBox.SelectedIndex > -1
                && _operatoriListBox.SelectedIndices.Count > 0
                && _descrizioneText.Text.Length > 0)
                _confermaButton.Enabled = true;
            else _confermaButton.Enabled = false;
        }
    }
}
