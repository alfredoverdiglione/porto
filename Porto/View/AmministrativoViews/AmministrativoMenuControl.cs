﻿using System;
using System.Windows.Forms;
using Porto.Model.Utenti;
using Porto.Controller.Interfacce;
using Porto.Model;

namespace Porto.View.AmministrativoViews
{
    public partial class AmministrativoMenuControl : UserControl {
        public event Logout Logout;

        private readonly DipendenteAmministrativo _admin;
        private readonly Panel _mainPanel;
        private readonly Model.Porto _portoAmministrato;
        private readonly DipendenteAmministrativoController _controller;

        public AmministrativoMenuControl() {
            InitializeComponent();
            _logOutButton.Click += OnLogout;
        }


        public AmministrativoMenuControl(DipendenteAmministrativo user, DipendenteAmministrativoController controller, Panel _mainPanel):this()
        {
            if (user == null || controller == null || _mainPanel == null)
                throw new ArgumentException("Almeno uno dei parametri di inizializzazione è nullo!");
            _admin = user;
            _controller = controller;
            _portoAmministrato = _controller.GestorePorti.OttieniPortoPerDipendente(_admin);
            this._mainPanel = _mainPanel;
            RefreshButtonStatus();
            /**Mi devo agganciare a tutti gli eventi che propone il nostro porto*/
            _portoAmministrato.SegnalazioneAdded += OnSegnalazioneAdded;
            _portoAmministrato.SegnalazioneRemoved += OnSegnalazioneRemoved;

            foreach (PostoBarca postoBarca in _portoAmministrato.PostiBarca)
            {
                postoBarca.PrenotazioneAdded += OnPrenotazioneEvent;
                postoBarca.PrenotazioneRemoved += OnPrenotazioneEvent;
            }

            /**Aggancio i vari listener*/
            _gestisciSegnalazioniButton.Click += TryGestisciSegnalazioni;
            _gestisciPrenotazioniButton.Click += TryGestisciPrenotazioni;
            _gestisciAttivitàButton.Click += TryGestisciAttività;
        }

        private void TryGestisciAttività(object sender, EventArgs e)
        {
            GestisciAttivitàControl gestisciAttività = new GestisciAttivitàControl(_admin, _controller);
            _mainPanel.Controls.Clear();
            _mainPanel.Controls.Add(gestisciAttività);
        }

        private void TryGestisciPrenotazioni(object sender, EventArgs e)
        {
            GestisciPrenotazioniAmministratoreControl gestisciPrenotazioni = new GestisciPrenotazioniAmministratoreControl(_portoAmministrato,_controller);
            _mainPanel.Controls.Clear();
            _mainPanel.Controls.Add(gestisciPrenotazioni);
        }

        private void OnSegnalazioneRemoved(object sender, EventArgs e)
        {
            RefreshButtonStatus();
        }

        private void OnPrenotazioneEvent(object sender, EventArgs e)
        {
            RefreshButtonStatus();
        }

        private void OnSegnalazioneAdded(object sender, EventArgs e)
        {
            RefreshButtonStatus();
        }

        private void TryGestisciSegnalazioni(object sender, EventArgs e)
        {
            GestisciSegnalazioniControl segnalazioniControl = new GestisciSegnalazioniControl(_admin,_controller);
            _mainPanel.Controls.Clear();
            _mainPanel.Controls.Add(segnalazioniControl);


        }

        private void RefreshButtonStatus()
        {
            _gestisciAttivitàButton.Enabled = _portoAmministrato.Segnalazioni.Count > 0 ? true : false;
            _gestisciPrenotazioniButton.Enabled = _portoAmministrato.PrenotazioniTotali > 0 ? true : false;
            _gestisciSegnalazioniButton.Enabled = _portoAmministrato.Segnalazioni.Count > 0 ? true : false;
        }

        private void OnLogout(object sender, EventArgs e)
        {
            if (Logout != null) Logout();
        }


    }
}
