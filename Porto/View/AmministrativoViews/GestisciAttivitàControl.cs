﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Porto.Controller.Interfacce;
using Porto.Model.Utenti;
using Porto.Model;

namespace Porto.View.AmministrativoViews
{
    public delegate bool CancellaAttività(String adminCF, Attività attività);
    public partial class GestisciAttivitàControl : UserControl {

        public event CancellaAttività CancellaAttività;
        private DipendenteAmministrativo _admin;
        private Model.Porto _portoGestito;
        private DipendenteAmministrativoController _controller;

        public GestisciAttivitàControl() {
            InitializeComponent();
        }

        public GestisciAttivitàControl(DipendenteAmministrativo _admin, DipendenteAmministrativoController _controller):this()
        {
            if (_admin == null || _controller == null)
                throw new ArgumentException("Almeno uno dei parametri di inizializzazione è nullo!");
            this._admin = _admin;
            this._controller = _controller;
            _portoGestito = _controller.GestorePorti.OttieniPortoPerDipendente(_admin);
            /**Mi lego agli eventi del modello, che nel nostro caso sono quelli dell'admin*/
            _portoGestito.AttivitàAdded += OnAttivitaChanged;
            _portoGestito.AttivitàRemoved += OnAttivitaChanged;

            _creaAttivitàButton.Click += TryCreaAttività;
            _attivitàListBox.DataSource = _portoGestito.TutteAttivitàPorto;
            _attivitàListBox.SelectedIndex = -1;

            _selezionaButton.Click += OnSeleziona;
            _eliminaButton.Click += TryElimina;
            _modificaButton.Click += TryModifica;
            CancellaAttività += _controller.RimuoviAttività;

            RefreshDetailsPanel();
            RefreshButtons();
            RefreshListBox();
        }

        private void TryModifica(object sender, EventArgs e)
        {
            if (_attivitàListBox.SelectedIndex > -1)
            {
                Attività a = (Attività)_attivitàListBox.SelectedItem;
                ModificaAttivitàControl modificaAttività = new ModificaAttivitàControl(_admin, a,_controller.GestorePorti.OttieniPortoPerDipendente(_admin),_controller);
                Form dialog = new Form();
                dialog.Size = new Size(400, 600);
                dialog.Controls.Add(modificaAttività);
                modificaAttività.Close += dialog.Close;
                dialog.Show();

            }
        }

        private void TryElimina(object sender, EventArgs e)
        {
            if(_attivitàListBox.SelectedIndex > -1)
            {
                if(CancellaAttività!=null &&
                    MessageBox.Show("Sei sicuro di voler eliminare l'attività?", "Conferma", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    CancellaAttività(_admin.CodiceFiscale, (Attività)_attivitàListBox.SelectedItem);
                }
            }
            RefreshListBox();
            RefreshDetailsPanel();
        }

        private void OnSeleziona(object sender, EventArgs e)
        {
            if (_attivitàListBox.SelectedIndex > -1)
            {
                Attività selezionata = (Attività)_attivitàListBox.SelectedItem;
                _descrizioneTextBox.Text = ""+selezionata.Descrizione;

                
                _operatoriListBox.DataSource = selezionata.Operatori;
                _operatoriListBox.DisplayMember = "NomeCognome";
                _operatoriListBox.SelectedIndex = -1;

                _segnalazioneAuthorText.Text = "Autore: "+selezionata.Segnalazione.Autore.NomeCognome;
                _segnalazioneDescriptionText.Text = ""+selezionata.Segnalazione.Descrizione;
                _eliminaButton.Enabled = true;
                _modificaButton.Enabled = true;
            }
        }

        private void RefreshDetailsPanel()
        {
            _descrizioneTextBox.Text = "";
            _operatoriListBox.DataSource = null;
            _operatoriListBox.SelectedIndex = -1;

            _segnalazioneAuthorText.Text = "";
            _segnalazioneDescriptionText.Text = "";
            _eliminaButton.Enabled = false;
            _modificaButton.Enabled = false;
        }

        private void OnAttivitaChanged(object sender, EventArgs e)
        {
            
            /**Devo cambiare il modello dei dati che in questo caso è l'admin visto che qualcosa è cambiato al suo interno*/
            foreach (DipendenteAmministrativo newAdmin in _controller.GestoreUtenti.Amministratori)
                if (newAdmin.CodiceFiscale == _admin.CodiceFiscale)
                    _admin = newAdmin;
            /**Aggiorno anche il modello interno del porto*/
            _portoGestito = _controller.GestorePorti.OttieniPortoPerDipendente(_admin);
            RefreshListBox();
            RefreshDetailsPanel();
        }

        private void RefreshListBox()
        {
            _attivitàListBox.DataSource = null;
            _attivitàListBox.DataSource = _portoGestito.TutteAttivitàPorto;
            _attivitàListBox.DisplayMember= "Descrizione";
            _attivitàListBox.SelectedIndex = -1;

        }

        private void RefreshButtons()
        {
            _eliminaButton.Enabled = false;
            _modificaButton.Enabled = false;
        }

        private void TryCreaAttività(object sender, EventArgs e)
        {
            Form form = new Form();
            form.Size = new Size(400, 400);
            form.AutoScaleMode = AutoScaleMode.None;
            CreaAttivitàControl newAttività = new CreaAttivitàControl(_portoGestito.Segnalazioni
                ,_admin,_controller);
            form.Controls.Add(newAttività);
            newAttività.Close += form.Close;
            form.ShowDialog();
        }
    }
}
