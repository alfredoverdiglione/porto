﻿namespace Porto.View.AmministrativoViews
{
    partial class CreaAttivitàControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._segnalazioneListBox = new System.Windows.Forms.ListBox();
            this._operatoriListBox = new System.Windows.Forms.ListBox();
            this._confermaButton = new System.Windows.Forms.Button();
            this._cancelButton = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this._descrizioneText = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // _segnalazioneListBox
            // 
            this._segnalazioneListBox.FormattingEnabled = true;
            this._segnalazioneListBox.Location = new System.Drawing.Point(4, 26);
            this._segnalazioneListBox.Name = "_segnalazioneListBox";
            this._segnalazioneListBox.Size = new System.Drawing.Size(294, 43);
            this._segnalazioneListBox.TabIndex = 0;
            // 
            // _operatoriListBox
            // 
            this._operatoriListBox.FormattingEnabled = true;
            this._operatoriListBox.Location = new System.Drawing.Point(4, 95);
            this._operatoriListBox.Name = "_operatoriListBox";
            this._operatoriListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this._operatoriListBox.Size = new System.Drawing.Size(293, 56);
            this._operatoriListBox.TabIndex = 1;
            // 
            // _confermaButton
            // 
            this._confermaButton.Location = new System.Drawing.Point(141, 274);
            this._confermaButton.Name = "_confermaButton";
            this._confermaButton.Size = new System.Drawing.Size(75, 23);
            this._confermaButton.TabIndex = 2;
            this._confermaButton.Text = "Conferma";
            this._confermaButton.UseVisualStyleBackColor = true;
            // 
            // _cancelButton
            // 
            this._cancelButton.Location = new System.Drawing.Point(222, 274);
            this._cancelButton.Name = "_cancelButton";
            this._cancelButton.Size = new System.Drawing.Size(75, 23);
            this._cancelButton.TabIndex = 3;
            this._cancelButton.Text = "Annulla";
            this._cancelButton.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(4, 0);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(143, 20);
            this.textBox1.TabIndex = 4;
            this.textBox1.Text = "Seleziona Segnalazione";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(4, 75);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(143, 20);
            this.textBox2.TabIndex = 5;
            this.textBox2.Text = "Seleziona Operatori";
            // 
            // _descrizioneText
            // 
            this._descrizioneText.Location = new System.Drawing.Point(4, 158);
            this._descrizioneText.Multiline = true;
            this._descrizioneText.Name = "_descrizioneText";
            this._descrizioneText.Size = new System.Drawing.Size(293, 110);
            this._descrizioneText.TabIndex = 6;
            // 
            // CreaAttivitàControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this._descrizioneText);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this._cancelButton);
            this.Controls.Add(this._confermaButton);
            this.Controls.Add(this._operatoriListBox);
            this.Controls.Add(this._segnalazioneListBox);
            this.MinimumSize = new System.Drawing.Size(300, 300);
            this.Name = "CreaAttivitàControl";
            this.Size = new System.Drawing.Size(300, 300);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox _segnalazioneListBox;
        private System.Windows.Forms.ListBox _operatoriListBox;
        private System.Windows.Forms.Button _confermaButton;
        private System.Windows.Forms.Button _cancelButton;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox _descrizioneText;
    }
}
