﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Porto.View {

    public delegate void LoginEvent(string username, string password);


    public partial class LogInControl : UserControl {
        public event LoginEvent Login;

        public LogInControl() {
            InitializeComponent();
            LoginButton.Click += OnLogin;
            Dock = DockStyle.Fill;
        }

        private void OnLogin(object sender, EventArgs e)
        {
            if (Login != null && !String.IsNullOrEmpty(Username) && !String.IsNullOrEmpty(Username))
                Login(Username, Password);
        }

        public Button LoginButton
        {
            get
            {
                return _loginButton;
            }
        }

        public String Username
        {
            get
            {
                return _usernameText.Text;
            }
        }

        public String Password
        {
            get
            {
                return _passwordText.Text;
            }
        }
    }
}
