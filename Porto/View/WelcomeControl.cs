﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Porto.Model.Utenti;

namespace Porto.View {

    public partial class WelcomeControl : UserControl {
        public WelcomeControl(Utente u) {
            InitializeComponent();
            _qualificaLabel.Text = u.Nome + " " + u.Cognome;

            
        }
    }
}
