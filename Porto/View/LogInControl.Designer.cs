﻿namespace Porto.View {
    partial class LogInControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this._label1 = new System.Windows.Forms.Label();
            this._usernameText = new System.Windows.Forms.TextBox();
            this._passwordText = new System.Windows.Forms.TextBox();
            this._loginButton = new System.Windows.Forms.Button();
            this._usernameLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _label1
            // 
            this._label1.AutoSize = true;
            this._label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._label1.Location = new System.Drawing.Point(141, 50);
            this._label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._label1.Name = "_label1";
            this._label1.Size = new System.Drawing.Size(155, 26);
            this._label1.TabIndex = 0;
            this._label1.Text = "Autenticazione";
            // 
            // _usernameText
            // 
            this._usernameText.Location = new System.Drawing.Point(98, 166);
            this._usernameText.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._usernameText.Name = "_usernameText";
            this._usernameText.Size = new System.Drawing.Size(229, 20);
            this._usernameText.TabIndex = 1;
            // 
            // _passwordText
            // 
            this._passwordText.Location = new System.Drawing.Point(98, 262);
            this._passwordText.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._passwordText.Name = "_passwordText";
            this._passwordText.PasswordChar = '*';
            this._passwordText.Size = new System.Drawing.Size(229, 20);
            this._passwordText.TabIndex = 2;
            // 
            // _loginButton
            // 
            this._loginButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._loginButton.Location = new System.Drawing.Point(126, 337);
            this._loginButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._loginButton.Name = "_loginButton";
            this._loginButton.Size = new System.Drawing.Size(168, 49);
            this._loginButton.TabIndex = 3;
            this._loginButton.Text = "Log In";
            this._loginButton.UseVisualStyleBackColor = true;
            // 
            // _usernameLabel
            // 
            this._usernameLabel.AutoSize = true;
            this._usernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._usernameLabel.Location = new System.Drawing.Point(94, 143);
            this._usernameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._usernameLabel.Name = "_usernameLabel";
            this._usernameLabel.Size = new System.Drawing.Size(145, 20);
            this._usernameLabel.TabIndex = 4;
            this._usernameLabel.Text = "Inserisci Username";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(98, 239);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Inserisci Password";
            // 
            // LogInControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.label2);
            this.Controls.Add(this._usernameLabel);
            this.Controls.Add(this._loginButton);
            this.Controls.Add(this._passwordText);
            this.Controls.Add(this._usernameText);
            this.Controls.Add(this._label1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "LogInControl";
            this.Size = new System.Drawing.Size(450, 488);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _label1;
        private System.Windows.Forms.TextBox _usernameText;
        private System.Windows.Forms.TextBox _passwordText;
        private System.Windows.Forms.Button _loginButton;
        private System.Windows.Forms.Label _usernameLabel;
        private System.Windows.Forms.Label label2;
    }
}
