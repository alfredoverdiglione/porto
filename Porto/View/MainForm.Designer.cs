﻿namespace Porto.View {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this._mainPanel = new System.Windows.Forms.Panel();
            this._sidePanel = new System.Windows.Forms.Panel();
            this._iconPictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this._iconPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // _mainPanel
            // 
            this._mainPanel.BackColor = System.Drawing.Color.Transparent;
            this._mainPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this._mainPanel.Location = new System.Drawing.Point(154, 0);
            this._mainPanel.Margin = new System.Windows.Forms.Padding(2);
            this._mainPanel.Name = "_mainPanel";
            this._mainPanel.Size = new System.Drawing.Size(530, 661);
            this._mainPanel.TabIndex = 0;
            // 
            // _sidePanel
            // 
            this._sidePanel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._sidePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(79)))), ((int)(((byte)(254)))));
            this._sidePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sidePanel.Location = new System.Drawing.Point(0, 126);
            this._sidePanel.Margin = new System.Windows.Forms.Padding(2);
            this._sidePanel.Name = "_sidePanel";
            this._sidePanel.Size = new System.Drawing.Size(150, 535);
            this._sidePanel.TabIndex = 1;
            // 
            // _iconPictureBox
            // 
            this._iconPictureBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(79)))), ((int)(((byte)(254)))));
            this._iconPictureBox.Image = global::Porto.Properties.Resources.ProgramIcon;
            this._iconPictureBox.Location = new System.Drawing.Point(0, 0);
            this._iconPictureBox.Name = "_iconPictureBox";
            this._iconPictureBox.Size = new System.Drawing.Size(150, 121);
            this._iconPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this._iconPictureBox.TabIndex = 2;
            this._iconPictureBox.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(109)))), ((int)(((byte)(254)))));
            this.ClientSize = new System.Drawing.Size(684, 661);
            this.Controls.Add(this._iconPictureBox);
            this.Controls.Add(this._sidePanel);
            this.Controls.Add(this._mainPanel);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimumSize = new System.Drawing.Size(700, 700);
            this.Name = "MainForm";
            this.Text = "Easy Sail";
            ((System.ComponentModel.ISupportInitialize)(this._iconPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel _mainPanel;
        private System.Windows.Forms.Panel _sidePanel;
        private System.Windows.Forms.PictureBox _iconPictureBox;
    }
}