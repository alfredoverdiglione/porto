﻿using System;
using System.Windows.Forms;
using Porto.Controller.Interfacce;
using Porto.Model.Utenti;
using Porto.Model;

namespace Porto.View
{
    public delegate bool DeletePrenotazione(Prenotazione p);
    public partial class GestisciPrenotazioniClienteControl : UserControl {
        private Cliente _client;
        private ClienteController _controller;
        public event DeletePrenotazione DeletePrenotazione;

        public GestisciPrenotazioniClienteControl() {
            InitializeComponent();
        }

        public GestisciPrenotazioniClienteControl(Cliente _client, ClienteController _controller):this()
        {
            if (_client == null || _controller == null)
                throw new ArgumentException("Almeno uno dei parametri di inizializzazione è nullo!");
            this._client = _client;
            this._controller = _controller;
            /**Popolo e setto la lista di prenotazioni del mio cliente*/
            _prenotazioniListBox.DataSource = _controller.GestorePorti.OttieniTuttePrenotazioniPerCliente(_client);
            _prenotazioniListBox.SelectedIndex = -1;
            _prenotazioniListBox.SelectedIndexChanged += ResetOptionalBehaviour;
            _selezionaButton.Click += OnPrenotazioneSelect;
            _deleteButton.Click += OnTryDelete;
            /**Aggancio il controller all'evento di cancellazione*/
            DeletePrenotazione += _controller.CancellaPrenotazione;
            ResetLabels();
        }

        

        private void OnTryDelete(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show( "Sei sicuro di voler cancellare la prenotazione?", "Continuare?", MessageBoxButtons.YesNo);
            if(result== DialogResult.Yes)
            {
                if (DeletePrenotazione != null &&
                    DeletePrenotazione((Prenotazione)_prenotazioniListBox.SelectedItem));
                MessageBox.Show("Cancellazione andata a buon fine!");
                RefreshPrenotazioniList();
            }
        }

        private void RefreshPrenotazioniList()
        {
            _prenotazioniListBox.DataSource = null;
            _prenotazioniListBox.DataSource = _controller.GestorePorti.OttieniTuttePrenotazioniPerCliente(_client);
            _prenotazioniListBox.SelectedIndex = -1;
            ResetOptionalBehaviour(null,null);
        }

        private void ResetOptionalBehaviour(object sender, EventArgs e)
        {
            ResetLabels();
            _deleteButton.Enabled = false;
        }

        private void ResetLabels()
        {
            _barcaLabel.Text = "Barca: ";
            _idLabel.Text = "Id: " ;
            _fineLabel.Text = "Data Fine: " ;
            _inizioLabel.Text = "Data Inizio: " ;
            _postoBarcaLabel.Text = "Posto Barca: ";
            _prezzoLabel.Text = "Prezzo: " ;
            _statusLabel.Text = "Stato Prenotazione: ";
            _portoLabel.Text = "Porto : ";
        }

        private void OnPrenotazioneSelect(object sender, EventArgs e)
        {
            if (_prenotazioniListBox.SelectedIndex > -1)
            {
                Prenotazione selectedPrenotazione = (Prenotazione)_prenotazioniListBox.SelectedItem;
                _barcaLabel.Text = "Barca: " + selectedPrenotazione.Barca;
                _idLabel.Text = "Id: " + selectedPrenotazione.Id;
                _fineLabel.Text = "Data Fine: " + selectedPrenotazione.DataFine;
                _inizioLabel.Text = "Data Inizio: " + selectedPrenotazione.DataInizio;
                _postoBarcaLabel.Text = "Posto Barca: " + selectedPrenotazione.PostoBarca;
                _prezzoLabel.Text = "Prezzo: " + selectedPrenotazione.Prezzo;
                _statusLabel.Text = "Stato Prenotazione: " + selectedPrenotazione.StatoPrenotazione;
                _portoLabel.Text = "Porto: " + selectedPrenotazione.PostoBarca.Porto;
                _deleteButton.Enabled = true;
            }
        }
    }
}
