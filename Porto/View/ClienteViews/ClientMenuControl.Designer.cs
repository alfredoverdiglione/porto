﻿namespace Porto.View {
    partial class ClientMenuControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this._prenotaButton = new System.Windows.Forms.Button();
            this._gestisciPrenotazioniButton = new System.Windows.Forms.Button();
            this._gestisciImbarcazioniButton = new System.Windows.Forms.Button();
            this._segnalaButton = new System.Windows.Forms.Button();
            this._logOutButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _prenotaButton
            // 
            this._prenotaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._prenotaButton.Location = new System.Drawing.Point(2, 2);
            this._prenotaButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._prenotaButton.Name = "_prenotaButton";
            this._prenotaButton.Size = new System.Drawing.Size(146, 57);
            this._prenotaButton.TabIndex = 0;
            this._prenotaButton.Text = "Prenota";
            this._prenotaButton.UseVisualStyleBackColor = true;
            // 
            // _gestisciPrenotazioniButton
            // 
            this._gestisciPrenotazioniButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._gestisciPrenotazioniButton.Location = new System.Drawing.Point(2, 64);
            this._gestisciPrenotazioniButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._gestisciPrenotazioniButton.Name = "_gestisciPrenotazioniButton";
            this._gestisciPrenotazioniButton.Size = new System.Drawing.Size(146, 57);
            this._gestisciPrenotazioniButton.TabIndex = 1;
            this._gestisciPrenotazioniButton.Text = "Gestisci Prenotazioni";
            this._gestisciPrenotazioniButton.UseVisualStyleBackColor = true;
            // 
            // _gestisciImbarcazioniButton
            // 
            this._gestisciImbarcazioniButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._gestisciImbarcazioniButton.Location = new System.Drawing.Point(2, 126);
            this._gestisciImbarcazioniButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._gestisciImbarcazioniButton.Name = "_gestisciImbarcazioniButton";
            this._gestisciImbarcazioniButton.Size = new System.Drawing.Size(146, 57);
            this._gestisciImbarcazioniButton.TabIndex = 2;
            this._gestisciImbarcazioniButton.Text = "Gestisci Imbarcazioni";
            this._gestisciImbarcazioniButton.UseVisualStyleBackColor = true;
            // 
            // _segnalaButton
            // 
            this._segnalaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._segnalaButton.Location = new System.Drawing.Point(2, 188);
            this._segnalaButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._segnalaButton.Name = "_segnalaButton";
            this._segnalaButton.Size = new System.Drawing.Size(146, 57);
            this._segnalaButton.TabIndex = 3;
            this._segnalaButton.Text = "Segnala";
            this._segnalaButton.UseVisualStyleBackColor = true;
            // 
            // _logOutButton
            // 
            this._logOutButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._logOutButton.Location = new System.Drawing.Point(2, 428);
            this._logOutButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._logOutButton.Name = "_logOutButton";
            this._logOutButton.Size = new System.Drawing.Size(146, 57);
            this._logOutButton.TabIndex = 4;
            this._logOutButton.Text = "Log Out";
            this._logOutButton.UseVisualStyleBackColor = true;
            // 
            // ClientMenuControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this._logOutButton);
            this.Controls.Add(this._segnalaButton);
            this.Controls.Add(this._gestisciImbarcazioniButton);
            this.Controls.Add(this._gestisciPrenotazioniButton);
            this.Controls.Add(this._prenotaButton);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "ClientMenuControl";
            this.Size = new System.Drawing.Size(150, 488);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _prenotaButton;
        private System.Windows.Forms.Button _gestisciPrenotazioniButton;
        private System.Windows.Forms.Button _gestisciImbarcazioniButton;
        private System.Windows.Forms.Button _segnalaButton;
        private System.Windows.Forms.Button _logOutButton;
    }
}
