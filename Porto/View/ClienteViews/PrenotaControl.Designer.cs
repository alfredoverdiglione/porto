﻿namespace Porto.View {
    partial class PrenotaControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label3 = new System.Windows.Forms.Label();
            this._barcheListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this._portiListBox = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this._postiPicker = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this._fromPicker = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this._toPicker = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._prezzoTextBox = new System.Windows.Forms.TextBox();
            this._postoBarcaTextBox = new System.Windows.Forms.TextBox();
            this._toTextBox = new System.Windows.Forms.TextBox();
            this._fromTextBox = new System.Windows.Forms.TextBox();
            this._portoTextBox = new System.Windows.Forms.TextBox();
            this._barcaTextBox = new System.Windows.Forms.TextBox();
            this._confermaButton = new System.Windows.Forms.Button();
            this._serviziTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(2, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(210, 26);
            this.label3.TabIndex = 6;
            this.label3.Text = "Seleziona una barca";
            // 
            // _barcheListBox
            // 
            this._barcheListBox.FormattingEnabled = true;
            this._barcheListBox.Location = new System.Drawing.Point(7, 28);
            this._barcheListBox.Margin = new System.Windows.Forms.Padding(2);
            this._barcheListBox.Name = "_barcheListBox";
            this._barcheListBox.Size = new System.Drawing.Size(434, 43);
            this._barcheListBox.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(2, 72);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(193, 26);
            this.label1.TabIndex = 8;
            this.label1.Text = "Seleziona un porto";
            // 
            // _portiListBox
            // 
            this._portiListBox.FormattingEnabled = true;
            this._portiListBox.Location = new System.Drawing.Point(7, 100);
            this._portiListBox.Margin = new System.Windows.Forms.Padding(2);
            this._portiListBox.Name = "_portiListBox";
            this._portiListBox.Size = new System.Drawing.Size(434, 43);
            this._portiListBox.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(2, 190);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(197, 26);
            this.label2.TabIndex = 10;
            this.label2.Text = "Seleziona un posto";
            // 
            // _postiPicker
            // 
            this._postiPicker.FormattingEnabled = true;
            this._postiPicker.Location = new System.Drawing.Point(7, 218);
            this._postiPicker.Margin = new System.Windows.Forms.Padding(2);
            this._postiPicker.Name = "_postiPicker";
            this._postiPicker.Size = new System.Drawing.Size(434, 95);
            this._postiPicker.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(2, 145);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(216, 26);
            this.label4.TabIndex = 12;
            this.label4.Text = "Seleziona un periodo";
            // 
            // _fromPicker
            // 
            this._fromPicker.Location = new System.Drawing.Point(32, 170);
            this._fromPicker.Margin = new System.Windows.Forms.Padding(2);
            this._fromPicker.MinDate = new System.DateTime(2016, 10, 30, 0, 0, 0, 0);
            this._fromPicker.Name = "_fromPicker";
            this._fromPicker.Size = new System.Drawing.Size(151, 20);
            this._fromPicker.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(4, 170);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 17);
            this.label5.TabIndex = 14;
            this.label5.Text = "Da";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(188, 170);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 17);
            this.label6.TabIndex = 15;
            this.label6.Text = "A";
            // 
            // _toPicker
            // 
            this._toPicker.Location = new System.Drawing.Point(207, 170);
            this._toPicker.Margin = new System.Windows.Forms.Padding(2);
            this._toPicker.MinDate = new System.DateTime(2016, 10, 31, 0, 0, 0, 0);
            this._toPicker.Name = "_toPicker";
            this._toPicker.Size = new System.Drawing.Size(151, 20);
            this._toPicker.TabIndex = 16;
            this._toPicker.Value = new System.DateTime(2016, 10, 31, 0, 0, 0, 0);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._serviziTextBox);
            this.groupBox1.Controls.Add(this._prezzoTextBox);
            this.groupBox1.Controls.Add(this._postoBarcaTextBox);
            this.groupBox1.Controls.Add(this._toTextBox);
            this.groupBox1.Controls.Add(this._fromTextBox);
            this.groupBox1.Controls.Add(this._portoTextBox);
            this.groupBox1.Controls.Add(this._barcaTextBox);
            this.groupBox1.Location = new System.Drawing.Point(7, 317);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(394, 246);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dettagli selezione";
            // 
            // _prezzoTextBox
            // 
            this._prezzoTextBox.Location = new System.Drawing.Point(5, 117);
            this._prezzoTextBox.Name = "_prezzoTextBox";
            this._prezzoTextBox.ReadOnly = true;
            this._prezzoTextBox.Size = new System.Drawing.Size(384, 20);
            this._prezzoTextBox.TabIndex = 5;
            // 
            // _postoBarcaTextBox
            // 
            this._postoBarcaTextBox.Location = new System.Drawing.Point(5, 143);
            this._postoBarcaTextBox.Name = "_postoBarcaTextBox";
            this._postoBarcaTextBox.ReadOnly = true;
            this._postoBarcaTextBox.Size = new System.Drawing.Size(384, 20);
            this._postoBarcaTextBox.TabIndex = 4;
            // 
            // _toTextBox
            // 
            this._toTextBox.Location = new System.Drawing.Point(5, 96);
            this._toTextBox.Name = "_toTextBox";
            this._toTextBox.ReadOnly = true;
            this._toTextBox.Size = new System.Drawing.Size(384, 20);
            this._toTextBox.TabIndex = 3;
            // 
            // _fromTextBox
            // 
            this._fromTextBox.Location = new System.Drawing.Point(5, 70);
            this._fromTextBox.Name = "_fromTextBox";
            this._fromTextBox.ReadOnly = true;
            this._fromTextBox.Size = new System.Drawing.Size(384, 20);
            this._fromTextBox.TabIndex = 2;
            // 
            // _portoTextBox
            // 
            this._portoTextBox.Location = new System.Drawing.Point(5, 44);
            this._portoTextBox.Name = "_portoTextBox";
            this._portoTextBox.ReadOnly = true;
            this._portoTextBox.Size = new System.Drawing.Size(384, 20);
            this._portoTextBox.TabIndex = 1;
            // 
            // _barcaTextBox
            // 
            this._barcaTextBox.Location = new System.Drawing.Point(5, 18);
            this._barcaTextBox.Name = "_barcaTextBox";
            this._barcaTextBox.ReadOnly = true;
            this._barcaTextBox.Size = new System.Drawing.Size(384, 20);
            this._barcaTextBox.TabIndex = 0;
            // 
            // _confermaButton
            // 
            this._confermaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._confermaButton.Location = new System.Drawing.Point(405, 504);
            this._confermaButton.Margin = new System.Windows.Forms.Padding(2);
            this._confermaButton.Name = "_confermaButton";
            this._confermaButton.Size = new System.Drawing.Size(97, 59);
            this._confermaButton.TabIndex = 18;
            this._confermaButton.Text = "Conferma";
            this._confermaButton.UseVisualStyleBackColor = true;
            // 
            // _serviziTextBox
            // 
            this._serviziTextBox.Location = new System.Drawing.Point(5, 169);
            this._serviziTextBox.Multiline = true;
            this._serviziTextBox.Name = "_serviziTextBox";
            this._serviziTextBox.ReadOnly = true;
            this._serviziTextBox.Size = new System.Drawing.Size(384, 72);
            this._serviziTextBox.TabIndex = 6;
            // 
            // PrenotaControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this._confermaButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this._toPicker);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this._fromPicker);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._postiPicker);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._portiListBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._barcheListBox);
            this.Controls.Add(this.label3);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "PrenotaControl";
            this.Size = new System.Drawing.Size(504, 565);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox _barcheListBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox _portiListBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox _postiPicker;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker _fromPicker;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker _toPicker;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button _confermaButton;
        private System.Windows.Forms.TextBox _barcaTextBox;
        private System.Windows.Forms.TextBox _postoBarcaTextBox;
        private System.Windows.Forms.TextBox _toTextBox;
        private System.Windows.Forms.TextBox _fromTextBox;
        private System.Windows.Forms.TextBox _portoTextBox;
        private System.Windows.Forms.TextBox _prezzoTextBox;
        private System.Windows.Forms.TextBox _serviziTextBox;
    }
}
