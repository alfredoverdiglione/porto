﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Porto.Model;
using Porto.Model.Utenti;
using Porto.Controller.Interfacce;

namespace Porto.View
{
    public delegate bool PrenotaPostoBarca(PostoBarca postoBarca, Barca barca, DateTime from, DateTime to);

    public partial class PrenotaControl : UserControl {
        private Cliente _cliente;
        private ClienteController _controller;
        public event PrenotaPostoBarca PrenotaPostoBarca;

        public PrenotaControl() {
            InitializeComponent();
        }

        public PrenotaControl(Cliente cliente, ClienteController _controller):this()
        {
            if (cliente == null || _controller == null)
                throw new ArgumentException("Almeno uno dei parametri di inizializzazione è nullo!");
            this._cliente = cliente;
            this._controller = _controller;
            PrenotaPostoBarca += _controller.Prenota;

            /**Popolo la listbox delle barche*/
            _barcheListBox.DataSource = _cliente.Barche;
            _barcheListBox.SelectedIndex = -1;
            _barcheListBox.SelectedIndexChanged += OnCheckPosti;
            _cliente.BarcaAdded += RefreshBarcheList;
            /**Popolo la listbox dei porti*/
            _portiListBox.DataSource = _controller.GestorePorti.PortiGestiti;
            _portiListBox.SelectedIndexChanged += OnCheckPosti;
            _portiListBox.SelectedIndex = -1;

            foreach (Model.Porto porto in _controller.GestorePorti.PortiGestiti)
            {
                foreach (PostoBarca pb in porto.PostiBarca)
                {
                    pb.PrenotazioneAdded += OnModelChanged;
                    pb.PrenotazioneRemoved += OnModelChanged;
                }
            }
            /**Setto per bene il DatePicker*/
            _fromPicker.ValueChanged += OnValueChanged;
            _fromPicker.MinDate = DateTime.Today;
            _toPicker.MinDate = DateTime.Today.AddDays(1);
            _toPicker.Value = _fromPicker.Value.AddDays(1);
            _fromPicker.ValueChanged += OnCheckPosti;
            _toPicker.ValueChanged += OnCheckPosti; 
            /**Disabilito il bottone prenota*/
            _confermaButton.Enabled = false;
            _confermaButton.Click += OnPrenota;
            RefreshTextBox();
        }

        private void OnModelChanged(object sender, EventArgs e)
        {
            RefreshPortoList();
        }

        private void RefreshPortoList()
        {
            _portiListBox.DataSource = null;
            _portiListBox.DataSource = _controller.GestorePorti.PortiGestiti;
            _portiListBox.SelectedIndex = -1;
        }

        private void OnPrenota(object sender, EventArgs e)
        {
            if (PrenotaPostoBarca != null &&
                _postiPicker.SelectedIndex>=0 &&
                _barcheListBox.SelectedIndex>=0 &&
                _portiListBox.SelectedIndex>=0)
            if(PrenotaPostoBarca((PostoBarca)_postiPicker.SelectedItem, (Barca)_barcheListBox.SelectedItem, _fromPicker.Value, _toPicker.Value))
                    MessageBox.Show("Prenotazione andata a buon fine!");
            RefreshPanel();
            RefreshPortoList();
        }

        private void RefreshPanel()
        {
            _portiListBox.SelectedIndex = -1;
            _barcheListBox.SelectedIndex = -1;
            _fromPicker.Value = DateTime.Today;
            _postiPicker.DataSource = null;
            RefreshTextBox();
        }

        private void OnSelection(object sender, EventArgs e)
        {
            PostoBarca chosen = (PostoBarca)_postiPicker.SelectedItem;
            if (chosen != null)
            {
                _confermaButton.Enabled = true;
                _barcaTextBox.Text = "Barca: " + ((Barca)_barcheListBox.SelectedItem);
                _portoTextBox.Text = "Porto: " + ((Model.Porto)_portiListBox.SelectedItem);
                _fromTextBox.Text = "Dal: " + (_fromPicker.Value);
                _toTextBox.Text = "Al: " + (_toPicker.Value);
                _prezzoTextBox.Text = "Prezzo Giornaliero: " + ((PostoBarca)_postiPicker.SelectedItem).PrezzoGiornaliero;
                _postoBarcaTextBox.Text = "Posto Barca: " + ((PostoBarca)_postiPicker.SelectedItem);
                _serviziTextBox.Text = "Servizi disponibili: ";
                foreach (Servizio servizio in chosen.Servizi)
                    _serviziTextBox.Text += servizio.Descrizione;
            }
        }

        private void RefreshTextBox()
        {
            _barcaTextBox.Text = "Barca: ";
            _portoTextBox.Text = "Porto: " ;
            _fromTextBox.Text = "Dal: " ;
            _toTextBox.Text = "Al: ";
            _prezzoTextBox.Text = "Prezzo Giornaliero: ";
            _postoBarcaTextBox.Text = "Posto Barca: ";
            _serviziTextBox.Text = "Servizi disponibili: ";
        }

        private void OnCheckPosti(object sender, EventArgs e)
        {
            if (_barcheListBox.SelectedIndex >= 0 && _portiListBox.SelectedIndex >= 0)
            {
                Model.Porto selectedPorto = (Model.Porto)_portiListBox.SelectedItem;
                Barca selectedBarca = (Barca)_barcheListBox.SelectedItem;
                List<PostoBarca> trovati = _controller.FiltraPosti(selectedBarca,selectedPorto,_fromPicker.Value,_toPicker.Value);
                if (trovati.Count > 0)
                {
                    _postiPicker.DataSource = null;
                    _postiPicker.DataSource = trovati;
                    _postiPicker.SelectedIndex = -1;
                    _postiPicker.SelectedIndexChanged += OnSelection;
                    _confermaButton.Enabled = false;
                    RefreshTextBox();
                }

            }
        }

        private void OnValueChanged(object sender, EventArgs e)
        {
            _toPicker.MinDate = _fromPicker.Value.AddDays(1);
            _toPicker.Value = _fromPicker.Value.AddDays(1);
            
        }

        private void RefreshBarcheList(object sender, EventArgs e)
        {
            _barcheListBox.DataSource = null;
            _barcheListBox.DataSource = _cliente.Barche;
            _barcheListBox.Refresh();
            _barcheListBox.SelectedIndex = -1;
        }
    }
}
