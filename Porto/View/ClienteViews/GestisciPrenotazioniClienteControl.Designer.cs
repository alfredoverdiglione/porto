﻿namespace Porto.View {
    partial class GestisciPrenotazioniClienteControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label3 = new System.Windows.Forms.Label();
            this._prenotazioniListBox = new System.Windows.Forms.ListBox();
            this._selezionaButton = new System.Windows.Forms.Button();
            this._deleteButton = new System.Windows.Forms.Button();
            this._detailsPanel = new System.Windows.Forms.Panel();
            this._portoLabel = new System.Windows.Forms.TextBox();
            this._statusLabel = new System.Windows.Forms.TextBox();
            this._prezzoLabel = new System.Windows.Forms.TextBox();
            this._fineLabel = new System.Windows.Forms.TextBox();
            this._barcaLabel = new System.Windows.Forms.TextBox();
            this._idLabel = new System.Windows.Forms.TextBox();
            this._postoBarcaLabel = new System.Windows.Forms.TextBox();
            this._inizioLabel = new System.Windows.Forms.TextBox();
            this._detailsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(2, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(234, 26);
            this.label3.TabIndex = 7;
            this.label3.Text = "Lista delle prenotazioni";
            // 
            // _prenotazioniListBox
            // 
            this._prenotazioniListBox.FormattingEnabled = true;
            this._prenotazioniListBox.Location = new System.Drawing.Point(7, 28);
            this._prenotazioniListBox.Margin = new System.Windows.Forms.Padding(2);
            this._prenotazioniListBox.Name = "_prenotazioniListBox";
            this._prenotazioniListBox.Size = new System.Drawing.Size(263, 82);
            this._prenotazioniListBox.TabIndex = 8;
            // 
            // _selezionaButton
            // 
            this._selezionaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._selezionaButton.Location = new System.Drawing.Point(274, 28);
            this._selezionaButton.Margin = new System.Windows.Forms.Padding(2);
            this._selezionaButton.Name = "_selezionaButton";
            this._selezionaButton.Size = new System.Drawing.Size(174, 35);
            this._selezionaButton.TabIndex = 19;
            this._selezionaButton.Text = "Seleziona";
            this._selezionaButton.UseVisualStyleBackColor = true;
            // 
            // _deleteButton
            // 
            this._deleteButton.Enabled = false;
            this._deleteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._deleteButton.Location = new System.Drawing.Point(276, 69);
            this._deleteButton.Name = "_deleteButton";
            this._deleteButton.Size = new System.Drawing.Size(171, 38);
            this._deleteButton.TabIndex = 20;
            this._deleteButton.Text = "Cancella";
            this._deleteButton.UseVisualStyleBackColor = true;
            // 
            // _detailsPanel
            // 
            this._detailsPanel.Controls.Add(this._portoLabel);
            this._detailsPanel.Controls.Add(this._statusLabel);
            this._detailsPanel.Controls.Add(this._prezzoLabel);
            this._detailsPanel.Controls.Add(this._fineLabel);
            this._detailsPanel.Controls.Add(this._barcaLabel);
            this._detailsPanel.Controls.Add(this._idLabel);
            this._detailsPanel.Controls.Add(this._postoBarcaLabel);
            this._detailsPanel.Controls.Add(this._inizioLabel);
            this._detailsPanel.Location = new System.Drawing.Point(7, 116);
            this._detailsPanel.Name = "_detailsPanel";
            this._detailsPanel.Size = new System.Drawing.Size(440, 369);
            this._detailsPanel.TabIndex = 21;
            // 
            // _portoLabel
            // 
            this._portoLabel.Location = new System.Drawing.Point(3, 82);
            this._portoLabel.Name = "_portoLabel";
            this._portoLabel.ReadOnly = true;
            this._portoLabel.Size = new System.Drawing.Size(433, 20);
            this._portoLabel.TabIndex = 7;
            // 
            // _statusLabel
            // 
            this._statusLabel.Location = new System.Drawing.Point(3, 160);
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.ReadOnly = true;
            this._statusLabel.Size = new System.Drawing.Size(433, 20);
            this._statusLabel.TabIndex = 6;
            // 
            // _prezzoLabel
            // 
            this._prezzoLabel.Location = new System.Drawing.Point(4, 134);
            this._prezzoLabel.Name = "_prezzoLabel";
            this._prezzoLabel.ReadOnly = true;
            this._prezzoLabel.Size = new System.Drawing.Size(433, 20);
            this._prezzoLabel.TabIndex = 5;
            // 
            // _fineLabel
            // 
            this._fineLabel.Location = new System.Drawing.Point(214, 4);
            this._fineLabel.Name = "_fineLabel";
            this._fineLabel.ReadOnly = true;
            this._fineLabel.Size = new System.Drawing.Size(222, 20);
            this._fineLabel.TabIndex = 4;
            // 
            // _barcaLabel
            // 
            this._barcaLabel.Location = new System.Drawing.Point(3, 56);
            this._barcaLabel.Name = "_barcaLabel";
            this._barcaLabel.ReadOnly = true;
            this._barcaLabel.Size = new System.Drawing.Size(433, 20);
            this._barcaLabel.TabIndex = 3;
            // 
            // _idLabel
            // 
            this._idLabel.Location = new System.Drawing.Point(3, 30);
            this._idLabel.Name = "_idLabel";
            this._idLabel.ReadOnly = true;
            this._idLabel.Size = new System.Drawing.Size(433, 20);
            this._idLabel.TabIndex = 2;
            // 
            // _postoBarcaLabel
            // 
            this._postoBarcaLabel.Location = new System.Drawing.Point(4, 108);
            this._postoBarcaLabel.Name = "_postoBarcaLabel";
            this._postoBarcaLabel.ReadOnly = true;
            this._postoBarcaLabel.Size = new System.Drawing.Size(433, 20);
            this._postoBarcaLabel.TabIndex = 1;
            // 
            // _inizioLabel
            // 
            this._inizioLabel.Location = new System.Drawing.Point(4, 4);
            this._inizioLabel.Name = "_inizioLabel";
            this._inizioLabel.ReadOnly = true;
            this._inizioLabel.Size = new System.Drawing.Size(204, 20);
            this._inizioLabel.TabIndex = 0;
            // 
            // GestisciPrenotazioniClienteControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this._detailsPanel);
            this.Controls.Add(this._deleteButton);
            this.Controls.Add(this._selezionaButton);
            this.Controls.Add(this._prenotazioniListBox);
            this.Controls.Add(this.label3);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "GestisciPrenotazioniClienteControl";
            this.Size = new System.Drawing.Size(450, 488);
            this._detailsPanel.ResumeLayout(false);
            this._detailsPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox _prenotazioniListBox;
        private System.Windows.Forms.Button _selezionaButton;
        private System.Windows.Forms.Button _deleteButton;
        private System.Windows.Forms.Panel _detailsPanel;
        private System.Windows.Forms.TextBox _fineLabel;
        private System.Windows.Forms.TextBox _barcaLabel;
        private System.Windows.Forms.TextBox _idLabel;
        private System.Windows.Forms.TextBox _postoBarcaLabel;
        private System.Windows.Forms.TextBox _inizioLabel;
        private System.Windows.Forms.TextBox _prezzoLabel;
        private System.Windows.Forms.TextBox _statusLabel;
        private System.Windows.Forms.TextBox _portoLabel;
    }
}
