﻿namespace Porto.View
{
    partial class InserimentoBarcaDialog
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._nomeTextBox = new System.Windows.Forms.TextBox();
            this._targaTextBox = new System.Windows.Forms.TextBox();
            this._lunghezzaTextBox = new System.Windows.Forms.TextBox();
            this._confirmButton = new System.Windows.Forms.Button();
            this._annullaButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Targa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Lunghezza";
            // 
            // _nomeTextBox
            // 
            this._nomeTextBox.Location = new System.Drawing.Point(109, 26);
            this._nomeTextBox.Name = "_nomeTextBox";
            this._nomeTextBox.Size = new System.Drawing.Size(232, 22);
            this._nomeTextBox.TabIndex = 3;
            // 
            // _targaTextBox
            // 
            this._targaTextBox.Location = new System.Drawing.Point(109, 53);
            this._targaTextBox.Name = "_targaTextBox";
            this._targaTextBox.Size = new System.Drawing.Size(232, 22);
            this._targaTextBox.TabIndex = 4;
            // 
            // _lunghezzaTextBox
            // 
            this._lunghezzaTextBox.Location = new System.Drawing.Point(109, 81);
            this._lunghezzaTextBox.Name = "_lunghezzaTextBox";
            this._lunghezzaTextBox.Size = new System.Drawing.Size(232, 22);
            this._lunghezzaTextBox.TabIndex = 5;
            // 
            // _confirmButton
            // 
            this._confirmButton.Location = new System.Drawing.Point(163, 120);
            this._confirmButton.Name = "_confirmButton";
            this._confirmButton.Size = new System.Drawing.Size(84, 23);
            this._confirmButton.TabIndex = 6;
            this._confirmButton.Text = "Conferma";
            this._confirmButton.UseVisualStyleBackColor = true;
            // 
            // _annullaButton
            // 
            this._annullaButton.Location = new System.Drawing.Point(253, 120);
            this._annullaButton.Name = "_annullaButton";
            this._annullaButton.Size = new System.Drawing.Size(84, 23);
            this._annullaButton.TabIndex = 7;
            this._annullaButton.Text = "Annulla";
            this._annullaButton.UseVisualStyleBackColor = true;
            // 
            // InserimentoBarcaDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._annullaButton);
            this.Controls.Add(this._confirmButton);
            this.Controls.Add(this._lunghezzaTextBox);
            this.Controls.Add(this._targaTextBox);
            this.Controls.Add(this._nomeTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "InserimentoBarcaDialog";
            this.Size = new System.Drawing.Size(344, 146);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox _nomeTextBox;
        private System.Windows.Forms.TextBox _targaTextBox;
        private System.Windows.Forms.TextBox _lunghezzaTextBox;
        private System.Windows.Forms.Button _confirmButton;
        private System.Windows.Forms.Button _annullaButton;
    }
}
