﻿using System;
using System.Windows.Forms;
using Porto.Model.Utenti;
using Porto.Controller.Interfacce;
using Porto.Model;
using System.Collections.Generic;

namespace Porto.View
{

    public partial class ClientMenuControl : UserControl {

        public event Logout Logout;
        private readonly Cliente _client;
        private readonly Panel _workingPanel;
        private readonly ClienteController _controller;

        public ClientMenuControl(Cliente c, Panel workingPanel,ClienteController controller) {

            if (c == null || controller == null || workingPanel==null)
                throw new ArgumentException("Almeno uno dei parametri di inizializzazione è nullo!");

            InitializeComponent();

            _logOutButton.Click += OnLogout;
            _client = c;
            _workingPanel = workingPanel;
            _controller = controller;

            /*Mi lego agli eventi di mio interesse*/
            _client.BarcaAdded += OnBarcaAdded;
            _client.BarcaRemoved += OnBarcaAdded;

            /**Inizializzo le azioni da eseguire in caso dei click sui pulsanti:*/
            _segnalaButton.Click += OnTrySegnala;
            _gestisciImbarcazioniButton.Click += OnTryImbarcazioni;
            _gestisciPrenotazioniButton.Click += OnTryPrenotazioni;
            _prenotaButton.Click += OnPrenota;

            SetUpListener();
            RefreshButtons();

        }

        private void OnBarcaAdded(object sender, EventArgs e)
        {
            RefreshButtons();
        }

        private void OnTryPrenotazioni(object sender, EventArgs e)
        {
            GestisciPrenotazioniClienteControl controlGestione = new GestisciPrenotazioniClienteControl(_client,_controller);
            _workingPanel.Controls.Clear();
            _workingPanel.Controls.Add(controlGestione);
        }

        private void SetUpListener()
        {
            foreach (Model.Porto porto in _controller.GestorePorti.PortiGestiti)
            {
                foreach (PostoBarca postoBarca in porto.PostiBarca)
                {
                    postoBarca.PrenotazioneAdded += OnPrenotazioneChanged;
                    postoBarca.PrenotazioneRemoved += OnPrenotazioneChanged;
                 }
            }
        }

        private void OnPrenota(object sender, EventArgs e)
        {
            PrenotaControl prenotaControl = new PrenotaControl(_client,_controller);
            _workingPanel.Controls.Clear();
            _workingPanel.Controls.Add(prenotaControl);   
        }

        private void OnTryImbarcazioni(object sender, EventArgs e)
        {
            GestisciImbarcazioniControl control = new GestisciImbarcazioniControl(_client,_controller);
            _workingPanel.Controls.Clear();
            _workingPanel.Controls.Add(control);
        }

        private void OnPrenotazioneChanged(object sender, EventArgs e)
        {
            RefreshButtons();
        }

        private void RefreshButtons()
        {
            List<Prenotazione> prenotazioni = _controller.GestorePorti.OttieniTuttePrenotazioniPerCliente(_client);
            bool oneConfirmed = false;
            foreach (Prenotazione p in prenotazioni)
                if(p.StatoPrenotazione==StatoPrenotazione.Confermata)
                    oneConfirmed = true;
            _segnalaButton.Enabled = oneConfirmed;
            _gestisciPrenotazioniButton.Enabled = _controller.GestorePorti.OttieniTuttePrenotazioniPerCliente(_client).Count < 1 ? false : true;
            _prenotaButton.Enabled = _client.Barche.Count < 1 ? false : true;

        }

        private void OnTrySegnala(object sender, EventArgs e)
        {
            _workingPanel.Controls.Clear();
            SegnalaControl segnalaControl = new SegnalaControl(_client,_controller);
            _workingPanel.Controls.Add(segnalaControl);
        }

        private void OnLogout(object sender, EventArgs e)
        {
            if (Logout != null) Logout();
          
        }
    }
}
