﻿namespace Porto.View {
    partial class GestisciImbarcazioniControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label3 = new System.Windows.Forms.Label();
            this._inserisciButton = new System.Windows.Forms.Button();
            this._modificaButton = new System.Windows.Forms.Button();
            this._eliminaButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this._nameTextBox = new System.Windows.Forms.TextBox();
            this._barcheListBox = new System.Windows.Forms.ListBox();
            this._selezioneButton = new System.Windows.Forms.Button();
            this._targaText = new System.Windows.Forms.TextBox();
            this._lunghezzaTextBox = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(2, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(278, 26);
            this.label3.TabIndex = 9;
            this.label3.Text = "Gestisci le tue imbarcazioni";
            // 
            // _inserisciButton
            // 
            this._inserisciButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._inserisciButton.Location = new System.Drawing.Point(0, 34);
            this._inserisciButton.Margin = new System.Windows.Forms.Padding(2);
            this._inserisciButton.Name = "_inserisciButton";
            this._inserisciButton.Size = new System.Drawing.Size(147, 30);
            this._inserisciButton.TabIndex = 11;
            this._inserisciButton.Text = "Inserisci Barca";
            this._inserisciButton.UseVisualStyleBackColor = true;
            // 
            // _modificaButton
            // 
            this._modificaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._modificaButton.Location = new System.Drawing.Point(298, 45);
            this._modificaButton.Margin = new System.Windows.Forms.Padding(2);
            this._modificaButton.Name = "_modificaButton";
            this._modificaButton.Size = new System.Drawing.Size(100, 38);
            this._modificaButton.TabIndex = 13;
            this._modificaButton.Text = "Modifica";
            this._modificaButton.UseVisualStyleBackColor = true;
            // 
            // _eliminaButton
            // 
            this._eliminaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._eliminaButton.Location = new System.Drawing.Point(298, 3);
            this._eliminaButton.Margin = new System.Windows.Forms.Padding(2);
            this._eliminaButton.Name = "_eliminaButton";
            this._eliminaButton.Size = new System.Drawing.Size(100, 38);
            this._eliminaButton.TabIndex = 15;
            this._eliminaButton.Text = "Elimina";
            this._eliminaButton.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this._lunghezzaTextBox);
            this.panel1.Controls.Add(this._targaText);
            this.panel1.Controls.Add(this._nameTextBox);
            this.panel1.Controls.Add(this._modificaButton);
            this.panel1.Controls.Add(this._eliminaButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 175);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.MinimumSize = new System.Drawing.Size(250, 250);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(400, 250);
            this.panel1.TabIndex = 16;
            // 
            // _nameTextBox
            // 
            this._nameTextBox.Location = new System.Drawing.Point(4, 3);
            this._nameTextBox.Margin = new System.Windows.Forms.Padding(2);
            this._nameTextBox.Name = "_nameTextBox";
            this._nameTextBox.ReadOnly = true;
            this._nameTextBox.Size = new System.Drawing.Size(290, 20);
            this._nameTextBox.TabIndex = 16;
            // 
            // _barcheListBox
            // 
            this._barcheListBox.FormattingEnabled = true;
            this._barcheListBox.Location = new System.Drawing.Point(2, 69);
            this._barcheListBox.Margin = new System.Windows.Forms.Padding(2);
            this._barcheListBox.Name = "_barcheListBox";
            this._barcheListBox.Size = new System.Drawing.Size(396, 95);
            this._barcheListBox.TabIndex = 17;
            // 
            // _selezioneButton
            // 
            this._selezioneButton.Location = new System.Drawing.Point(304, 46);
            this._selezioneButton.Margin = new System.Windows.Forms.Padding(2);
            this._selezioneButton.Name = "_selezioneButton";
            this._selezioneButton.Size = new System.Drawing.Size(94, 19);
            this._selezioneButton.TabIndex = 18;
            this._selezioneButton.Text = "Seleziona Barca";
            this._selezioneButton.UseVisualStyleBackColor = true;
            // 
            // _targaText
            // 
            this._targaText.Location = new System.Drawing.Point(4, 27);
            this._targaText.Margin = new System.Windows.Forms.Padding(2);
            this._targaText.Name = "_targaText";
            this._targaText.ReadOnly = true;
            this._targaText.Size = new System.Drawing.Size(290, 20);
            this._targaText.TabIndex = 17;
            // 
            // _lunghezzaTextBox
            // 
            this._lunghezzaTextBox.Location = new System.Drawing.Point(4, 51);
            this._lunghezzaTextBox.Margin = new System.Windows.Forms.Padding(2);
            this._lunghezzaTextBox.Name = "_lunghezzaTextBox";
            this._lunghezzaTextBox.ReadOnly = true;
            this._lunghezzaTextBox.Size = new System.Drawing.Size(290, 20);
            this._lunghezzaTextBox.TabIndex = 18;
            // 
            // GestisciImbarcazioniControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this._selezioneButton);
            this.Controls.Add(this._barcheListBox);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this._inserisciButton);
            this.Controls.Add(this.label3);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "GestisciImbarcazioniControl";
            this.Size = new System.Drawing.Size(400, 425);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button _inserisciButton;
        private System.Windows.Forms.Button _modificaButton;
        private System.Windows.Forms.Button _eliminaButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListBox _barcheListBox;
        private System.Windows.Forms.TextBox _nameTextBox;
        private System.Windows.Forms.Button _selezioneButton;
        private System.Windows.Forms.TextBox _lunghezzaTextBox;
        private System.Windows.Forms.TextBox _targaText;
    }
}
