﻿using System;
using System.Windows.Forms;
using Porto.Model;
using Porto.Controller.Interfacce;

namespace Porto.View
{
    public delegate void ModificaBarca(Barca daModificare,String nuovaTarga,String nuovoNome);
    
    public partial class ModificaBarcaControl : UserControl {
        public event ModificaBarca ModificaBarca;
        public event CloseForm CloseForm;
        private readonly Barca _toModify;
        private readonly ClienteController _controller;

        public ModificaBarcaControl() {
            InitializeComponent();
        }

        public ModificaBarcaControl(Barca b,ClienteController controller):this()
        {
            if (b == null || controller == null)
                throw new ArgumentException("Almeno uno dei parametri di inizializzazione è nullo!");
            _toModify = b;
            _controller = controller;
            ModificaBarca += _controller.ModificaBarca;
            Dock = DockStyle.Fill;
            _nomeTextBox.KeyUp += CheckStatus;
            _targaTextBox.KeyUp += CheckStatus;
            _nomeTextBox.Text = _toModify.Nome;
            _targaTextBox.Text = _toModify.Targa;
            _confermaButton.Enabled = false;
            _confermaButton.Click += OnConferma;
        }

        private void CheckStatus(object sender, KeyEventArgs e)
        {
            if (!String.IsNullOrEmpty(_nomeTextBox.Text) &&
                !String.IsNullOrEmpty(_targaTextBox.Text))
                _confermaButton.Enabled = true;
        }

        private void OnConferma(object sender, EventArgs e)
        {
            if(MessageBox.Show("Sei sicuro di quello che fai?", "Conferma", MessageBoxButtons.YesNo)
                == DialogResult.Yes)
            {
                if (ModificaBarca != null)
                    ModificaBarca(_toModify, _targaTextBox.Text, _nomeTextBox.Text);
                if (CloseForm != null)
                    CloseForm();
            }
        }
    }
}
