﻿namespace Porto.View {
    partial class SegnalaControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this._portiListBox = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this._descrizioneTextBox = new System.Windows.Forms.RichTextBox();
            this._segnalaButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(2, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(193, 26);
            this.label1.TabIndex = 9;
            this.label1.Text = "Seleziona un porto";
            // 
            // _portiListBox
            // 
            this._portiListBox.FormattingEnabled = true;
            this._portiListBox.Location = new System.Drawing.Point(7, 28);
            this._portiListBox.Margin = new System.Windows.Forms.Padding(2);
            this._portiListBox.Name = "_portiListBox";
            this._portiListBox.Size = new System.Drawing.Size(434, 121);
            this._portiListBox.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(2, 150);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(250, 26);
            this.label2.TabIndex = 11;
            this.label2.Text = "Inserisci una descrizione";
            // 
            // _descrizioneTextBox
            // 
            this._descrizioneTextBox.Location = new System.Drawing.Point(7, 178);
            this._descrizioneTextBox.Margin = new System.Windows.Forms.Padding(2);
            this._descrizioneTextBox.Name = "_descrizioneTextBox";
            this._descrizioneTextBox.Size = new System.Drawing.Size(434, 242);
            this._descrizioneTextBox.TabIndex = 12;
            this._descrizioneTextBox.Text = "";
            // 
            // _segnalaButton
            // 
            this._segnalaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._segnalaButton.Location = new System.Drawing.Point(142, 424);
            this._segnalaButton.Margin = new System.Windows.Forms.Padding(2);
            this._segnalaButton.Name = "_segnalaButton";
            this._segnalaButton.Size = new System.Drawing.Size(150, 61);
            this._segnalaButton.TabIndex = 13;
            this._segnalaButton.Text = "Segnala";
            this._segnalaButton.UseVisualStyleBackColor = true;
            // 
            // SegnalaControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this._segnalaButton);
            this.Controls.Add(this._descrizioneTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._portiListBox);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "SegnalaControl";
            this.Size = new System.Drawing.Size(450, 488);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox _portiListBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox _descrizioneTextBox;
        private System.Windows.Forms.Button _segnalaButton;
    }
}
