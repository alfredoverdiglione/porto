﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Porto.Model.Utenti;
using Porto.Controller.Interfacce;
using Porto.Model;

namespace Porto.View
{
    public partial class GestisciImbarcazioniControl : UserControl {
        private readonly Cliente _cliente;
        private readonly ClienteController _controller;

        public GestisciImbarcazioniControl(Cliente cliente, ClienteController controller) {
            if (cliente == null || controller == null)
                throw new ArgumentException("Almeno uno dei parametri di inizializzazione è nullo!");

            InitializeComponent();

            _cliente = cliente;
            _controller = controller;

            _barcheListBox.DataSource = _cliente.Barche;
            _barcheListBox.DisplayMember = "Targa";
            _barcheListBox.SelectedIndex = -1;
           
            _selezioneButton.Click += RefreshDetails;

            _cliente.BarcaAdded += RefreshList;
            _eliminaButton.Enabled = false;
            _eliminaButton.Click += TryDelete;
            _modificaButton.Enabled = false;
            _modificaButton.Click += TryModifica;
            _inserisciButton.Click += TryInserisciBarca;

            _nameTextBox.Text = "Nome:";
            _targaText.Text = "Targa:";
            _lunghezzaTextBox.Text = "Lunghezza:";

            Dock = DockStyle.Fill;
        }

        private void TryModifica(object sender, EventArgs e)
        {
            ModificaBarcaControl modificaControl = new ModificaBarcaControl((Barca)_barcheListBox.SelectedItem, _controller);
            Form dialog = new Form();
            dialog.Size = new Size(500, 250);
            dialog.Text = "Modifica barca";
            dialog.Controls.Add(modificaControl);
            modificaControl.CloseForm += dialog.Close;
            dialog.ShowDialog();
            RefreshList(null,null);
        }

        private void TryDelete(object sender, EventArgs e)
        {
            if (_barcheListBox.SelectedIndex >= 0)
                _controller.TryRemoveBarca(_cliente, (Barca)_barcheListBox.SelectedItem);
            RefreshList(this, EventArgs.Empty);
        }

        private void RefreshList(object sender, EventArgs e)
        {
            _barcheListBox.DataSource = null;
            _barcheListBox.DataSource = _cliente.Barche;
            _barcheListBox.Update();

            _barcheListBox.SelectedIndex = -1;
            _nameTextBox.Text = "Nome:";
            _targaText.Text = "Targa:";
            _lunghezzaTextBox.Text = "Lunghezza:";
            _modificaButton.Enabled = false;
            _eliminaButton.Enabled = false;
        }

        private void TryInserisciBarca(object sender, EventArgs e)
        {

            InserimentoBarcaDialog dialog = new InserimentoBarcaDialog();

            dialog.InserisciBarca += OnConfirm;
            Form window = new Form();
            dialog.CloseForm += window.Close;
            window.Controls.Add(dialog);
            window.ShowDialog();
            
        }

        private void OnConfirm(string nomeBarca, string targa, float lunghezza)
        {
            Barca toAdd = new Barca(nomeBarca, targa, lunghezza);
            _controller.AggiungiBarca(_cliente,toAdd);
        }

        private void RefreshDetails(object sender, EventArgs e)
        {
            if (_barcheListBox.SelectedIndex >= 0)
            {
                Barca selected = ((Barca)_barcheListBox.SelectedItem);
                _nameTextBox.Text = "Nome: " + selected.Nome;
                _targaText.Text = "Targa: " + selected.Targa;
                _lunghezzaTextBox.Text = "Lunghezza: " +selected.Lunghezza;
                _eliminaButton.Enabled = true;
                _modificaButton.Enabled = true;
            }
        }
    }
}
