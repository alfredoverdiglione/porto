﻿using System;
using System.Linq;
using System.Windows.Forms;
using Porto.Model.Utenti;
using Porto.Controller.Interfacce;
using System.Collections.Generic;
using Porto.Model;

namespace Porto.View
{
    public delegate void Segnala(Cliente cliente, Model.Porto porto, String descrizione);

    public partial class SegnalaControl : UserControl {
        public Segnala Segnala;
        private Cliente _client;
        private ClienteController _controller;

        public SegnalaControl() {
            InitializeComponent();
        }

        public SegnalaControl(Cliente _client, ClienteController _controller):this()
        {
            if (_client == null || _controller == null)
                throw new ArgumentException("Almeno uno dei due parametri di inizializzazione è nullo!");
            this._client = _client;
            this._controller = _controller;
            Segnala += _controller.Segnala;

            SetUpListBox();
            
            _descrizioneTextBox.KeyUp += CheckStatus;
            _segnalaButton.Enabled = false;
            _segnalaButton.Click += TrySegnala;
        }

        private void SetUpListBox()
        {
            List<Model.Porto> portiPerSegnalazione = new List<Model.Porto>();
            foreach(Model.Porto p in _controller.GestorePorti.OttieniPortiPerCliente(_client))
            {
                foreach(Prenotazione prenotazione in _controller.GestorePorti.OttieniTuttePrenotazioniPerCliente(_client))
                {
                    if(prenotazione.StatoPrenotazione == StatoPrenotazione.Confermata)
                    {
                        portiPerSegnalazione.Add(p);
                        break;
                    }
                }
            }
            _portiListBox.DataSource =portiPerSegnalazione;
            _portiListBox.SelectedIndex = -1;
            _portiListBox.SelectedIndexChanged += CheckStatus;
        }

        private void TrySegnala(object sender, EventArgs e)
        {
            if (Segnala != null)
                Segnala(_client, (Model.Porto)_portiListBox.SelectedItem, _descrizioneTextBox.Text);
            MessageBox.Show("Segnalazione effettuata!");
            ClearDataEntry();
        }

        private void ClearDataEntry()
        {
            _portiListBox.SelectedIndex = -1;
            _descrizioneTextBox.Text = "";
        }

        private void CheckStatus(object sender, EventArgs e)
        {
            if (_portiListBox.SelectedIndex > -1 &&
                _descrizioneTextBox.Text.Count() > 0)
                _segnalaButton.Enabled = true;
        }
    }
}
