﻿using System;
using System.Windows.Forms;

namespace Porto.View
{
    public delegate void InserisciBarca(String nomeBarca, String targa, float lunghezza);
    public delegate void CloseForm();
    public partial class InserimentoBarcaDialog : UserControl
    {
        public event InserisciBarca InserisciBarca;
        public event CloseForm CloseForm;

        public InserimentoBarcaDialog()
        {
            InitializeComponent();
            _confirmButton.Click += OnConferma;
            _annullaButton.Click += OnAnnulla;
        }

        private void OnConferma(object sender, EventArgs e)
        {
            if (InserisciBarca != null && 
                !String.IsNullOrEmpty(_nomeTextBox.Text) &&
                !String.IsNullOrEmpty(_targaTextBox.Text)&&
                !String.IsNullOrEmpty(_lunghezzaTextBox.Text))
                InserisciBarca(_nomeTextBox.Text, _targaTextBox.Text, (float)Convert.ToDouble(_lunghezzaTextBox.Text));
            if(CloseForm!=null)
                CloseForm();
            Dispose();
            
        }

        private void OnAnnulla(object sender, EventArgs e)
        {
            if (CloseForm != null)
                CloseForm();
            Dispose();
        }
    }
}
