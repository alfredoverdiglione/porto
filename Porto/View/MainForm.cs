﻿using Porto.Controller.Interfacce;
using Porto.Model.Utenti;
using System;
using System.Windows.Forms;
using Porto.View.AmministrativoViews;
using Porto.Controller;

namespace Porto.View
{
    public partial class MainForm : Form {
        private GestoreUtenti _model;
        private BasicController _controller;
        private DipendenteAmministrativoController _adminController;
        private ClienteController _clienteController;


        public MainForm(GestoreUtenti model, BasicController controller, 
                        DipendenteAmministrativoController adminController, ClienteController clienteController) {
            if (model == null || controller == null)
                throw new ArgumentException("Model o controller non valido!");
            InitializeComponent();
            _model = model;
            _controller = controller;
            FirstInteraction();

            _adminController = adminController;
            _clienteController = clienteController;
            
            
        }

        private void FirstInteraction()
        {
            _mainPanel.Controls.Clear();
            _sidePanel.Controls.Clear();
            LogInControl loginControl = new LogInControl();
            loginControl.Login += OnLogin;
            _mainPanel.Controls.Add(loginControl);
        }

        private void OnLogin(string username, string password)
        {
           Utente u = _controller.LogIn(username, password);
           if (u != null)
            {
                if (u is Cliente) SetUp((Cliente)u);
                if (u is AssistenteDiTerra) SetUp((AssistenteDiTerra)u);
                if (u is DipendenteAmministrativo) SetUp((DipendenteAmministrativo)u);
            }
        }

        

        private void SetUp(AssistenteDiTerra u)
        {
            TerraMenuControl sideMenu = new TerraMenuControl(u,_mainPanel);
            _sidePanel.Controls.Add(sideMenu);
            _mainPanel.Controls.Clear();

            WelcomeControl welcome = new WelcomeControl(u);
           
            sideMenu.Logout += OnLogout;
            _mainPanel.Controls.Add(welcome);
        }

        private void SetUp(Cliente u)
        {
            /**Gli passo l'utente con cui poi andrò a lavorare e il pannello nel quale andare
             poi a creare i vari casi d'uso*/
                ClientMenuControl sideMenu = new ClientMenuControl((Cliente)u, _mainPanel, _clienteController);
                _sidePanel.Controls.Add(sideMenu);
                _mainPanel.Controls.Clear();

                WelcomeControl vista = new WelcomeControl(u);
                sideMenu.Logout += OnLogout;
                _mainPanel.Controls.Add(vista);
            
        }

        private void SetUp(DipendenteAmministrativo u)
        {
            /**Gli passo l'utente con cui poi andrò a lavorare e il pannello nel quale andare
             poi a creare i vari casi d'uso*/
                AmministrativoMenuControl sideMenu = new AmministrativoMenuControl((DipendenteAmministrativo)u, _adminController, _mainPanel);
                _sidePanel.Controls.Add(sideMenu);
                _mainPanel.Controls.Clear();

                WelcomeControl vista = new WelcomeControl(u);
                sideMenu.Logout += OnLogout;
                _mainPanel.Controls.Add(vista);
            
        }

        private void OnLogout()
        {
            FirstInteraction();
        }
    }
}
