﻿namespace Porto.View
{
    partial class AssistenteDiTerraView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._detailPanel = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._collaboratori = new System.Windows.Forms.ListBox();
            this._segnalazione = new System.Windows.Forms.TextBox();
            this._descrizione = new System.Windows.Forms.TextBox();
            this._dataCreazione = new System.Windows.Forms.TextBox();
            this._mansioniListBox = new System.Windows.Forms.ListBox();
            this._nameBox = new System.Windows.Forms.TextBox();
            this._selezionaButton = new System.Windows.Forms.Button();
            this._detailPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // _detailPanel
            // 
            this._detailPanel.Controls.Add(this.label4);
            this._detailPanel.Controls.Add(this.label3);
            this._detailPanel.Controls.Add(this.label2);
            this._detailPanel.Controls.Add(this.label1);
            this._detailPanel.Controls.Add(this._collaboratori);
            this._detailPanel.Controls.Add(this._segnalazione);
            this._detailPanel.Controls.Add(this._descrizione);
            this._detailPanel.Controls.Add(this._dataCreazione);
            this._detailPanel.Location = new System.Drawing.Point(4, 37);
            this._detailPanel.Name = "_detailPanel";
            this._detailPanel.Size = new System.Drawing.Size(485, 310);
            this._detailPanel.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(259, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Operatori Assegnati:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 168);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Segnalazione:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Descrizione:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Creata il: ";
            // 
            // _collaboratori
            // 
            this._collaboratori.FormattingEnabled = true;
            this._collaboratori.Location = new System.Drawing.Point(259, 51);
            this._collaboratori.Name = "_collaboratori";
            this._collaboratori.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this._collaboratori.Size = new System.Drawing.Size(223, 251);
            this._collaboratori.TabIndex = 3;
            // 
            // _segnalazione
            // 
            this._segnalazione.Location = new System.Drawing.Point(3, 184);
            this._segnalazione.Multiline = true;
            this._segnalazione.Name = "_segnalazione";
            this._segnalazione.ReadOnly = true;
            this._segnalazione.Size = new System.Drawing.Size(250, 123);
            this._segnalazione.TabIndex = 2;
            // 
            // _descrizione
            // 
            this._descrizione.Location = new System.Drawing.Point(3, 51);
            this._descrizione.Multiline = true;
            this._descrizione.Name = "_descrizione";
            this._descrizione.ReadOnly = true;
            this._descrizione.Size = new System.Drawing.Size(250, 114);
            this._descrizione.TabIndex = 1;
            // 
            // _dataCreazione
            // 
            this._dataCreazione.Location = new System.Drawing.Point(60, 0);
            this._dataCreazione.Name = "_dataCreazione";
            this._dataCreazione.ReadOnly = true;
            this._dataCreazione.Size = new System.Drawing.Size(131, 20);
            this._dataCreazione.TabIndex = 0;
            // 
            // _mansioniListBox
            // 
            this._mansioniListBox.FormattingEnabled = true;
            this._mansioniListBox.Location = new System.Drawing.Point(4, 4);
            this._mansioniListBox.Name = "_mansioniListBox";
            this._mansioniListBox.Size = new System.Drawing.Size(240, 30);
            this._mansioniListBox.TabIndex = 1;
            // 
            // _nameBox
            // 
            this._nameBox.Location = new System.Drawing.Point(339, 4);
            this._nameBox.Name = "_nameBox";
            this._nameBox.ReadOnly = true;
            this._nameBox.Size = new System.Drawing.Size(158, 20);
            this._nameBox.TabIndex = 3;
            // 
            // _selezionaButton
            // 
            this._selezionaButton.Location = new System.Drawing.Point(250, 8);
            this._selezionaButton.Name = "_selezionaButton";
            this._selezionaButton.Size = new System.Drawing.Size(75, 23);
            this._selezionaButton.TabIndex = 4;
            this._selezionaButton.Text = "Seleziona";
            this._selezionaButton.UseVisualStyleBackColor = true;
            // 
            // AssistenteDiTerraView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this._selezionaButton);
            this.Controls.Add(this._nameBox);
            this.Controls.Add(this._mansioniListBox);
            this.Controls.Add(this._detailPanel);
            this.MinimumSize = new System.Drawing.Size(500, 360);
            this.Name = "AssistenteDiTerraView";
            this.Size = new System.Drawing.Size(500, 360);
            this._detailPanel.ResumeLayout(false);
            this._detailPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel _detailPanel;
        private System.Windows.Forms.ListBox _mansioniListBox;
        private System.Windows.Forms.ListBox _collaboratori;
        private System.Windows.Forms.TextBox _segnalazione;
        private System.Windows.Forms.TextBox _descrizione;
        private System.Windows.Forms.TextBox _dataCreazione;
        private System.Windows.Forms.TextBox _nameBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button _selezionaButton;
    }
}
