﻿namespace Porto.View {
    partial class TerraMenuControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this._visualizzaAttivitàButton = new System.Windows.Forms.Button();
            this._logOutButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _visualizzaAttivitàButton
            // 
            this._visualizzaAttivitàButton.Enabled = false;
            this._visualizzaAttivitàButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._visualizzaAttivitàButton.Location = new System.Drawing.Point(2, 2);
            this._visualizzaAttivitàButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._visualizzaAttivitàButton.Name = "_visualizzaAttivitàButton";
            this._visualizzaAttivitàButton.Size = new System.Drawing.Size(146, 57);
            this._visualizzaAttivitàButton.TabIndex = 0;
            this._visualizzaAttivitàButton.Text = "Visualizza Attività";
            this._visualizzaAttivitàButton.UseVisualStyleBackColor = true;
            // 
            // _logOutButton
            // 
            this._logOutButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._logOutButton.Location = new System.Drawing.Point(2, 428);
            this._logOutButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this._logOutButton.Name = "_logOutButton";
            this._logOutButton.Size = new System.Drawing.Size(146, 57);
            this._logOutButton.TabIndex = 5;
            this._logOutButton.Text = "Log Out";
            this._logOutButton.UseVisualStyleBackColor = true;
            // 
            // TerraMenuControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this._logOutButton);
            this.Controls.Add(this._visualizzaAttivitàButton);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "TerraMenuControl";
            this.Size = new System.Drawing.Size(150, 488);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _visualizzaAttivitàButton;
        private System.Windows.Forms.Button _logOutButton;
        //private System.Windows.Forms.Button _logOutButton;
    }
}
