﻿using System;
using System.Windows.Forms;
using Porto.Model.Utenti;
using Porto.Model;

namespace Porto.View
{
    public delegate void Logout();
    public partial class AssistenteDiTerraView : UserControl
    {
        private readonly AssistenteDiTerra _utente;
        //private readonly Controller _controller;

        public AssistenteDiTerraView()
        {
            InitializeComponent();
           
        }

       
        public AssistenteDiTerraView(AssistenteDiTerra utente) : this()
        {
            if (utente == null)
                throw new ArgumentException("Utente nullo?");

            /*Non ho nessun evento a cui legarmi*/
            _utente = utente;

            _nameBox.Text = "Ciao, " + utente.Nome + " " + utente.Cognome;
            if (_utente.Attività.Count < 1)
                MessageBox.Show("Non hai attività programmate!");
            else
            {
                _mansioniListBox.DataSource = _utente.Attività;
                _mansioniListBox.DisplayMember = "Descrizione";
                _mansioniListBox.SelectedIndex = -1;
                _selezionaButton.Click += OnSelezione;
            }
        }

        public AssistenteDiTerra Utente
        {
            get
            {
                return _utente;
            }
        }

        private void OnSelezione(object sender, EventArgs e)
        {
            Attività selezionata = (Attività) _mansioniListBox.SelectedItem;
            _dataCreazione.Text = selezionata.DataCreazione + "" ;
            _descrizione.Text = selezionata.Descrizione;
            _segnalazione.Text = selezionata.Segnalazione.Descrizione;
            _collaboratori.DataSource = selezionata.Operatori;
            _collaboratori.DisplayMember = "NomeCognome";
        }

    }
}
