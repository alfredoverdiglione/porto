﻿using Porto.Model.Utenti;
using System;
using System.Windows.Forms;

namespace Porto.View
{
    public partial class TerraMenuControl : UserControl {
        public event Logout Logout;


        private AssistenteDiTerra _assistente;
        private Control _workingPanel;

        public TerraMenuControl(AssistenteDiTerra assistente,Control workingPanel) {
            if (assistente == null || workingPanel == null)
                throw new ArgumentException("Almeno un parametro di inizializzazione nullo!");
            InitializeComponent();
            _assistente = assistente;
            _workingPanel = workingPanel;

            RefreshButtons();
            _logOutButton.Click += OnLogout;
            _visualizzaAttivitàButton.Click += TryVisualizzaAttività;

        }

        private void TryVisualizzaAttività(object sender, EventArgs e)
        {
            _workingPanel.Controls.Clear();
            AssistenteDiTerraView visualizzaAttività = new AssistenteDiTerraView(_assistente);
            _workingPanel.Controls.Add(visualizzaAttività);
        }

        private void RefreshButtons()
        {
            _visualizzaAttivitàButton.Enabled = _assistente.Attività.Count > 0 ? true : false;
        }

        private void OnLogout(object sender, EventArgs e)
        {
            if (Logout != null) Logout();
        }
    }
}
