﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Porto.Model;
using Porto.Model.Utenti;
using Porto.Persistence.Interfacce;

namespace Porto.Persistence
{
    public class MockGestorePersistenza : GestorePersistenza
    {
        private List<Model.Porto> _porti;
        private List<Utente> _utenti;

        public MockGestorePersistenza()
        {
            DipendenteAmministrativo admin = new DipendenteAmministrativo("Alfredo", "Verdiglione", "VRDLRD94S11F537C", "veralfre", "111194");
            DipendenteAmministrativo admin2 = new DipendenteAmministrativo("Fortunato", "Fredo", "AAAAAAAAAAAAAAAE", "testE", "testE");
            AssistenteDiTerra operaio = new AssistenteDiTerra("Marco", "Bianchi", "AAAAAAAAAAAAAAAA", "test", "test");
            AssistenteDiTerra operaio2 = new AssistenteDiTerra("Carlo", "Rossi", "AAAAAAAAAAAAAAAB", "testA", "testA");
            AssistenteDiTerra operaio3 = new AssistenteDiTerra("Gianfranco", "Gialli", "AAAAAAAAAAAAAAAC", "testC", "testC");
            AssistenteDiTerra operaio4 = new AssistenteDiTerra("Marino", "Eligio", "AAAAAAAAAAAAAAAD", "testD", "testD");

            List<Dipendente> d = new List<Dipendente>() { admin,admin2, operaio, operaio3,operaio4 };
           

            Cliente c = new Cliente("Giovanni", "Verdi", "BBBBBBBBBBBBBBBB", "test2", "test2");
            Barca b = new Barca("Veliero dei 7 mari", "CICCIOGAMER", 10);
            c.AggiungiBarca(b);


            Model.Porto p = new Model.Porto("Porto di Tropea", "Via Marina del Vescovado", d);
            List<Dipendente> d2 = new List<Dipendente>() { operaio2 };
            Model.Porto porto2 = new Model.Porto("Porto di Salerno", "Via salerno",d2);

            PostoBarca p1 = new PostoBarca("A01", 50, 60, p);
            PostoBarca p2 = new PostoBarca("A02", 100, 200, p);



            List<PostoBarca> posti = new List<PostoBarca>() { p1, p2 };
            p.PostiBarca.AddRange(posti);

            PostoBarca p3 = new PostoBarca("A01", 50, 60, porto2);
            PostoBarca p4 = new PostoBarca("A02", 100, 200, porto2);

            List<PostoBarca> posti2 = new List<PostoBarca>() { p3, p4 };
            porto2.PostiBarca.AddRange(posti2);

            
            _porti = new List<Model.Porto>() { p, porto2 };

            

            

            Cliente c2 = new Cliente("Alessandro", "Gialli", "CCCCCCCCCCCCCCCC", "test3", "test3");
            Barca b2 = new Barca("Il veliero dei capitano Hacab", "WAZZAP", 20);
            //CAUSA ECCEZIONE A GIUSTA RAGIONE: c2.AggiungiBarca(b);
            c2.AggiungiBarca(b2);



            _utenti = new List<Utente>() { admin,admin2, c, c2, operaio, operaio2, operaio3,operaio4 };
            
        }

        public List<Model.Porto> LoadPorti(string fileName)
        {
            return _porti;
        }

        public List<Utente> LoadUtenti(string fileName)
        {
            return _utenti;
        }

        public bool SavePorti(List<Model.Porto> porti, string fileName)
        {
            /*Concretamente non salvo da nessuna parte*/
            return true;
        }

        public bool SaveUtenti(List<Utente> utenti, string fileName)
        {
            /*Concretamente non salvo da nessuna parte*/
            return true;
        }
    }
}
