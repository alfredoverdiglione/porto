﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Porto.Model;
using Porto.Model.Utenti;
using Porto.Persistence.Interfacce;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Porto.Persistence
{


    public class BinaryGestorePersistenza : GestorePersistenza
    {
        

        public List<Model.Porto> LoadPorti(string fileName)
        {
            if (String.IsNullOrEmpty(fileName))
                throw new ArgumentException("Nome file nullo!");
            return LoadObject(fileName) as List<Model.Porto>;
        }

        public List<Utente> LoadUtenti(string fileName)
        {
            if (String.IsNullOrEmpty(fileName))
                throw new ArgumentException("Nome file nullo!");
            return LoadObject(fileName) as List<Utente>;
        }

        public bool SavePorti(List<Model.Porto> porti, string fileName)
        {
            if (String.IsNullOrEmpty(fileName))
                throw new ArgumentException("Nome file nullo!");
            if (porti == null)
                throw new ArgumentException("Cosa devo salvare?");
            
            return SaveObject(porti,fileName);
        }

        public bool SaveUtenti(List<Utente> utenti, string fileName)
        {
            if (String.IsNullOrEmpty(fileName))
                throw new ArgumentException("Nome file nullo!");
            if (utenti == null)
                throw new ArgumentException("Cosa devo salvare?");
            

            return SaveObject(utenti,fileName);
        }

        private bool SaveObject(Object o,String fileName)
        {
           
            if (String.IsNullOrEmpty(fileName))
                throw new ArgumentException("Nome file nullo!");
            if (o == null)
                throw new ArgumentException("Cosa devo salvare?");

            //Serializzo il tutto nel file dato 
            using (FileStream file = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(file, o);
            }

            return true;
        }

        private Object LoadObject(String fileName)
        {
            if (String.IsNullOrEmpty(fileName))
                throw new ArgumentException("Nome file nullo!");
            
            Object result = null;
            using (FileStream file = new FileStream(fileName, FileMode.Open))
            {
                BinaryFormatter bf = new BinaryFormatter();
                result = bf.Deserialize(file);
            }
            return result;
        }

    }
}
