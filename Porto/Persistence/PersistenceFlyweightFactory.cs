﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Porto.Persistence.Interfacce;

namespace Porto.Persistence
{
    public enum PersistenceType
    {
        Mock,Binary
    }

    public static class PersistenceFlyweightFactory
    {
        /**Utilizzo un dizionario perchè è più comodo da gestire*/
        private static readonly Dictionary<PersistenceType, GestorePersistenza>
            _flyweights = new Dictionary<PersistenceType, GestorePersistenza>();


        public static GestorePersistenza GetPersister(PersistenceType tipo)
        {
            if(!_flyweights.ContainsKey(tipo))
            {
                switch (tipo)
                {
                    case PersistenceType.Mock: _flyweights[tipo] = new MockGestorePersistenza();
                                                                   break;
                    case PersistenceType.Binary: _flyweights[tipo] = new BinaryGestorePersistenza();
                                                                   break;
                }
            }
            return _flyweights[tipo];
        }

    }
}
