﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Porto.Model;
using Porto.Model.Utenti;

namespace Porto.Persistence.Interfacce
{
    public interface GestorePersistenza:GestorePersistenzaPorto,GestorePersistenzaUtente
    {
                      
    }
}
