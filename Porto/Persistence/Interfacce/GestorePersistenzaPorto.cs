﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Porto.Persistence.Interfacce
{
    public interface GestorePersistenzaPorto
    {
        List<Porto.Model.Porto> LoadPorti(String fileName);
        bool SavePorti(List<Model.Porto> porti, String fileName);

    }
}
