﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Porto.Model.Utenti;

namespace Porto.Persistence.Interfacce
{
    public interface PersistenzaUtente
    {
        List<Utente> LoadUtenti(String fileName);
        bool SaveUtenti(List<Utente> utenti, String fileName);
    }
}
