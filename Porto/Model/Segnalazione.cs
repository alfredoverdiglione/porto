﻿using Porto.Model.Utenti;
using System;

namespace Porto.Model
{
    [Serializable]
    public class Segnalazione
    {
        private readonly string _descrizione;
        private readonly Cliente _autore;

        public Segnalazione(Cliente autore, string descrizione)
        {
            if (string.IsNullOrEmpty(descrizione) || autore == null)
                throw new ArgumentException("Uno dei valori non è buono!");
            _descrizione = descrizione;
            _autore = autore;
        }

        public string Descrizione
        {
            get
            {
                return _descrizione;
            }
        }

        public Cliente Autore
        {
            get
            {
                return _autore;
            }
        }

        public override string ToString() {
            return _descrizione;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Segnalazione))
                return false;
            else
            {
                Segnalazione toCompare = (Segnalazione)obj;
                if (toCompare.Autore.CodiceFiscale == Autore.CodiceFiscale && toCompare.Descrizione == Descrizione)
                    return true;
                else return false;
            }
        }
    }
}
