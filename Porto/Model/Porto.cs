﻿using Porto.Model.Utenti;
using System;
using System.Collections.Generic;
using Porto.Model.Interfacce;

namespace Porto.Model
{

    [Serializable]
    public class Porto:IPorto
    {

        [field: NonSerialized]
        public event EventHandler AttivitàAdded;
        [field: NonSerialized]
        public event EventHandler AttivitàRemoved;

        private string _nome;
        private readonly string _indirizzo;
        private readonly List<Dipendente> _dipendenti;
        private List<PostoBarca> _postiBarca;
        private readonly List<Segnalazione> _segnalazioni;
        private readonly List<Attività> _attività;

        [field:NonSerializedAttribute]
        public event EventHandler SegnalazioneAdded;
        [field: NonSerializedAttribute]
        public event EventHandler SegnalazioneRemoved;

        public Porto(String nome,String indirizzo, List<Dipendente> dipendenti)
        {
            if (String.IsNullOrEmpty(nome) || String.IsNullOrEmpty(indirizzo))
                throw new ArgumentException("Una delle due stringhe è nulla o vuota!");
            if (dipendenti == null || dipendenti.Count < 1)
                throw new ArgumentException("Le liste dei posti barca e dei dipendenti non vanno bene!");

            foreach(Dipendente d in dipendenti)
            {
                if (d.LavoraPresso != null)
                    throw new ArgumentException("Uno dei dipendenti lavora già presso un altro porto!");
            }


            _nome = nome;
            _indirizzo = indirizzo;
            _dipendenti = dipendenti;

            foreach(Dipendente d in _dipendenti)
            {
                d.LavoraPresso = this;
            }

            _segnalazioni = new List<Segnalazione>();
            _postiBarca = new List<PostoBarca>();
            _attività = new List<Attività>();
        }


        public string Nome
        {
            get
            {
                return _nome;
            }
        }

        public string Indirizzo
        {
            get
            {
                return _indirizzo;
            }
        }

        public List<Dipendente> Dipendenti
        {
            get
            {
                return _dipendenti;
            }
        }

        public List<PostoBarca> PostiBarca
        {
            get
            {
                return _postiBarca;
            }
            set {
                List<PostoBarca> postiBarca = value;
                if (postiBarca.Count < 1 || postiBarca == null)
                    throw new ArgumentException("Parametri non validi");
                foreach(PostoBarca pb in value)
                {
                    if (pb.Porto.Nome != Nome)
                        throw new ArgumentException("Uno dei posti barca non appartiene a questo porto!");
                }
                _postiBarca = postiBarca;
            }
        }

        public List<Segnalazione> Segnalazioni
        {
            get
            {
                return _segnalazioni;
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Porto)) return false;
            else
            {
                Porto toCompare = (Porto)obj;
                if (toCompare.Nome == Nome && toCompare.Indirizzo == Indirizzo)
                    return true;
                else return false;
            }
        }


        public List<AssistenteDiTerra> AssistentiDiTerra
        {
            get
            {
                List<AssistenteDiTerra> result = new List<AssistenteDiTerra>();
                foreach (Dipendente d in Dipendenti)
                    if (d is AssistenteDiTerra) result.Add((AssistenteDiTerra)d);
                return result;
            }
        }

        public List<DipendenteAmministrativo> Amministratori
        {
            get
            {
                List<DipendenteAmministrativo> result = new List<DipendenteAmministrativo>();
                foreach (Dipendente d in Dipendenti)
                    if (d is DipendenteAmministrativo) result.Add((DipendenteAmministrativo)d);
                return result;
            }
        }

        public void Segnala(Segnalazione segnalazione)
        {
            if (segnalazione == null)
                throw new ArgumentException("Vuoi segnalare qualcosa di nullo?");
            _segnalazioni.Add(segnalazione);

            if (SegnalazioneAdded != null)
                SegnalazioneAdded(segnalazione, EventArgs.Empty);
            
        }

        public void RimuoviSegnalazione(Segnalazione segnalazione)
        {
            if (segnalazione == null)
                throw new ArgumentException("Vuoi segnalare qualcosa di nullo?");
            if (_segnalazioni.Contains(segnalazione))
                _segnalazioni.Remove(segnalazione);
            if (SegnalazioneRemoved != null)
                SegnalazioneRemoved(segnalazione, EventArgs.Empty);
        }

        public override string ToString() {
            return _nome;
        }

        public int PrenotazioniTotali
        {
            /**Restituisce il numero totale di prenotazione ATTUALE*/
            get
            {
                int value = 0;
                foreach (PostoBarca postoBarca in PostiBarca)
                    value += postoBarca.Storico.Prenotazioni.Count;
                return value;
            }
        }

        public List<Prenotazione> TuttePrenotazioni
        {
            get
            {
                List<Prenotazione> result = new List<Prenotazione>();
                foreach (PostoBarca pb in PostiBarca)
                    foreach (Prenotazione prenotazione in pb.Storico.Prenotazioni)
                        result.Add(prenotazione);
                return result;
            }
        }

        public List<Attività> OttieniAttivitàPerAssistente(AssistenteDiTerra assistente)
        {
            if (assistente == null)
                throw new ArgumentException("Per chi devo cercare le attività? [NULL]");

            List<Attività> result = new List<Attività>();
             foreach(Attività creataDa in TutteAttivitàPorto)
                    foreach(IAssistenteDiTerra operatore in creataDa.Operatori)
                    {
                        if(operatore.CodiceFiscale == assistente.CodiceFiscale)
                        {
                            result.Add(creataDa);
                            break;
                        }
                    }
            return result;
        }

        public List<Attività> TutteAttivitàPorto
        {
            get
            {
                return _attività;
            }
        }

        public bool CreaAttività(string descrizione, Segnalazione s, List<AssistenteDiTerra> operatori)
        {
            if (string.IsNullOrEmpty(descrizione) || s == null || operatori == null || operatori.Count < 1)
                throw new ArgumentException("Parametri di inizializzazione non validi!");
            bool presence = false;
            foreach (Segnalazione segnalazione in Segnalazioni)
                if (segnalazione.Equals(s))
                    presence = true;
            if (!presence)
                throw new ArgumentException("La segnalazione non appartiene al porto presso cui lavora l'amministratore!");

            /**Se è già presente una attività per quella segnalazione non faccio niente chiaramente,
               andrà poi a modificare*/
            foreach (Attività attività in TutteAttivitàPorto)
                if (attività.Segnalazione.Equals(s))
                    return false;

            foreach (AssistenteDiTerra assistente in operatori)
            {
                if (!assistente.LavoraPresso.Equals(this))
                    throw new ArgumentException("Un operaio non lavora presso questo porto!");
            }

            /*
            Converto gli utenti desiderati nel loro corrispettivo valore di interfaccia*/
            List<IAssistenteDiTerra> assistenti = new List<IAssistenteDiTerra>();
            foreach (AssistenteDiTerra assistente in operatori)
                assistenti.Add(assistente);

            //Vanno convertiti perchè io passo degli operatori, però la funzione di creazione delle attività vuole degli
            //IAssistentiDiTerra
            Attività a = new Attività(descrizione, assistenti, s);

            /**Aggiungo ad ogni assistente la attività così creata in maniera che così possano vederla*/
            foreach (AssistenteDiTerra assistente in operatori)
                assistente.Attività.Add(a);

            TutteAttivitàPorto.Add(a);

            if (AttivitàAdded != null)
                AttivitàAdded(this, EventArgs.Empty);
            return true;
        }


        public bool RimuoviAttività(Attività a)
        {
            if (a == null)
                throw new ArgumentException("Attività nulla!");
            bool presence = false;
            foreach (Attività attività in TutteAttivitàPorto)
                if (attività.Equals(a)) presence = true;
            if (!presence)
                throw new ArgumentException("La segnalazione non appartiene al porto presso cui lavora l'amministratore!");
            bool result = TutteAttivitàPorto.Remove(a);

            /**Devo anche rimuoverla da tutti gli assistenti l'operazione altrimenti
             mi rimane ad imperitura memoria.*/
            foreach (IAssistenteDiTerra iadt in a.Operatori)
                foreach(AssistenteDiTerra adt in AssistentiDiTerra)
                    if(iadt.CodiceFiscale == adt.CodiceFiscale)
                        adt.Attività.Remove(a);

            if (AttivitàRemoved != null)
                AttivitàRemoved(this, EventArgs.Empty);
            return result;
        }
    }
}
