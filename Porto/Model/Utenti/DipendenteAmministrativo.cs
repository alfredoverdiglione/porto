﻿using System;

namespace Porto.Model.Utenti
{

    [Serializable]
    public class DipendenteAmministrativo : Dipendente
    {
        
        

        public DipendenteAmministrativo(string nome, string cognome, string codiceFiscale, string username, string password) : base(nome, cognome, codiceFiscale, username, password)
        {
            
        }  

        
    }
}
