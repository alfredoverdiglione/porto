﻿using System;
using System.Collections.Generic;
using Porto.Model.Interfacce;

namespace Porto.Model.Utenti
{
    [Serializable]
    public class AssistenteDiTerra : Dipendente,IAssistenteDiTerra
    {

        private readonly List<Attività> _attività;

        public AssistenteDiTerra(string nome, string cognome, string codiceFiscale, string username, string password) : base(nome, cognome, codiceFiscale, username, password)
        {
            _attività = new List<Attività>();
        }

       
        public List<Attività> Attività
        {
            get
            {
                return _attività;
            }
        }
    }
}
