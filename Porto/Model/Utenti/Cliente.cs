﻿using System;
using System.Collections.Generic;
using Porto.Model.Interfacce;

namespace Porto.Model.Utenti
{
    [Serializable]
    public class Cliente : Utente,ICliente
    {
        private readonly List<Barca> _barche;

        [field: NonSerialized]
        public event EventHandler BarcaAdded;
        [field: NonSerialized]
        public event EventHandler BarcaRemoved;

        public Cliente(string nome, string cognome, string codiceFiscale, string username, string password) : base(nome, cognome, codiceFiscale, username, password)
        {
            _barche = new List<Barca>();
        }

        public List<Barca> Barche
        {
            get
            {
                return _barche;
            }
        }

        public bool AggiungiBarca(Barca b)
        {
            if (b == null || b.Proprietario != null)
                throw new ArgumentException("La barca non esiste oppure ha già un proprietario!");

            /**Aggiungo la barca e mi premuro di registrare il proprietario*/
            Barche.Add(b);
            b.Proprietario = this;

            if (BarcaAdded != null)
                BarcaAdded(b, EventArgs.Empty);

            return true;
        }

        public bool RimuoviBarca(Barca b)
        {
            if (b == null || !b.Proprietario.Equals(this))
                throw new ArgumentException("La barca non esiste o non sei il proprietario!");
            /**Non devo de-registrare perchè l'oggetto verrà distrutto dal GC*/
            Barche.Remove(b);
            if (BarcaRemoved != null)
                BarcaRemoved(b, null);
            return true;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Cliente))
                return false;
            else
            {
                Cliente toCompare = (Cliente)obj;
                if (toCompare.CodiceFiscale == CodiceFiscale) return true;
                else return false;
            }
        }

    }
}
