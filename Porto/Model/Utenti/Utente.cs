﻿using System;

namespace Porto.Model.Utenti
{
    [Serializable]
    public abstract class Utente
    {
        private readonly string _nome;
        private readonly string _cognome;
        private readonly string _codiceFiscale;
        private readonly string _username;
        private string _password;

        protected Utente(string nome, string cognome, string codiceFiscale, string username, string password)
        {
            if (String.IsNullOrEmpty(nome) || String.IsNullOrEmpty(cognome) || String.IsNullOrEmpty(codiceFiscale) ||
                String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password))
                throw new ArgumentException("Uno dei parametri di creazione è nullo o vuoto!");
            if (codiceFiscale.Length != 16)
                throw new ArgumentException("Il codice fiscale deve essere lungo 16 caratteri!");
            _nome = nome;
            _cognome = cognome;
            _codiceFiscale = codiceFiscale;
            _username = username;
            _password = password;
        }

        public string Nome
        {
            get
            {
                return _nome;
            }
        }

        public string Cognome
        {
            get
            {
                return _cognome;
            }
        }

        public string CodiceFiscale
        {
            get
            {
                return _codiceFiscale;
            }
        }

        public string Username
        {
            get
            {
                return _username;
            }
        }

        public string Password
        {
            get
            {
                return _password;
            }

            set
            {
                if (String.IsNullOrEmpty(value))
                    throw new ArgumentException("La password non può essere vuota o nulla!");
                _password = value;
            }
        }

        public String NomeCognome
        {
            get
            {
                return Nome + " " + Cognome;
            }
        }
    }
}
