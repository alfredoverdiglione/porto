﻿using System;
using Porto.Model.Interfacce;

namespace Porto.Model.Utenti
{
    [Serializable]
    public abstract class Dipendente : Utente
    {
        private IPorto _lavoraPresso;

       protected Dipendente(string nome, string cognome, string codiceFiscale, string username, string password) :
            base(nome, cognome, codiceFiscale, username, password) {
          
        }

       

        public IPorto LavoraPresso
        {
            get
            {
                return _lavoraPresso;
            }
            set
            {
                if (value == null || LavoraPresso != null)
                    throw new ArgumentException("Non puoi cambiare il porto presso cui lavori!");
                _lavoraPresso = value;
            }
        }

     }
}
