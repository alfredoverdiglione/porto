﻿using System;
using Porto.Model.Interfacce;

namespace Porto.Model
{
    [Serializable]
    public enum StatoPrenotazione
    {
        Confermata,NonConfermata
    }
    [Serializable]
    public class Prenotazione
    {
        private readonly string _id;
        private readonly DateTime _dataInizio;
        private readonly DateTime _dataFine;
        private StatoPrenotazione _statoPrenotazione;
        private readonly float _prezzo;
        private readonly Barca _barca;
        private readonly IPostoBarca _postoBarca;

        public Prenotazione(string id, DateTime inizio, DateTime fine, Barca barca, IPostoBarca posto) {
            if (string.IsNullOrEmpty(id) || inizio == null || fine == null || barca == null || posto == null)
                throw new ArgumentException("Parametri sbagliati");
            if (inizio > fine)
                throw new ArgumentException("L'intervallo di tempo non va bene");

            _id = id;
            _dataInizio = inizio;
            _dataFine = fine;
            /**Devo considerare che se sono meno di 24 ore la differenza non darà un giorno, ma 0. Idem per qualsiasi conto che non sia
               multiplo esatto di 24h.*/
           _prezzo = posto.PrezzoGiornaliero * ((fine - inizio).Days+1);
            _barca = barca;
            _postoBarca = posto;
            _statoPrenotazione = StatoPrenotazione.NonConfermata;
        }
       
        public string Id
        {
            get
            {
                return _id;
            }
        }

        public DateTime DataInizio
        {
            get
            {
                return _dataInizio;
            }
        }

        public DateTime DataFine
        {
            get
            {
                return _dataFine;
            }
        }

        public int NumeroGiorni
        {
            get
            {
                return (DataFine - DataInizio).Days + 1;
            }
        }

        public StatoPrenotazione StatoPrenotazione
        {
            get
            {
                return _statoPrenotazione;
            }

            set
            {
               _statoPrenotazione = value;
            }
        }

        public float Prezzo
        {
            get
            {
                return _prezzo;
            }
        }

        public Barca Barca
        {
            get
            {
                return _barca;
            }
        }

        public IPostoBarca PostoBarca
        {
            get
            {
                return _postoBarca;
            }
        }

      
        public void Conferma()
        {
            StatoPrenotazione = StatoPrenotazione.Confermata;
        }

        public bool Overlaps(Prenotazione toCompare)
        {
            if (toCompare == null)
                throw new ArgumentException("Prenotazione da confrontare nulla!");
            return Overlaps(toCompare.DataInizio, toCompare.DataFine);
        }

        public bool Overlaps(DateTime from,DateTime to)
        {
            if (from == null || to == null || from > to)
                throw new ArgumentException("Intervallo non valido!");
            /*** Identifico con A questa classe, con B le date che vengono fornite, l'algoritmo perciò in 
                 linguaggio sarà : (StartA <= EndB)  and  (EndA >= StartB)*/

            return DataInizio < to && DataFine > to;
        }

        public override string ToString() {
            return "" + _id;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Prenotazione))
                return false;
            else
            {
                Prenotazione toCompare = (Prenotazione)obj;
                if (toCompare.Id == Id)
                    return true;
                else return false;
            }
        }
    }
}
