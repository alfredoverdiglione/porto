﻿using System;
using System.Collections.Generic;
using Porto.Model.Interfacce;

namespace Porto.Model
{
    [Serializable]
    public class PostoBarca :IPostoBarca
    {
        
        private readonly string _id;
        private readonly float _lunghezzaMassima;
        private float _prezzoGiornaliero;
        private readonly List<Servizio> _servizi;
        private readonly Storico _storico;
        private readonly IPorto _porto;
        [field:NonSerialized]
        public event EventHandler PrenotazioneAdded;
        [field: NonSerialized]
        public event EventHandler PrenotazioneRemoved;

        public PostoBarca(string id, float lunghezzaMassima, float prezzoGiornaliero, IPorto porto) {
            if (string.IsNullOrEmpty(id) || lunghezzaMassima <= 0 || prezzoGiornaliero < 0 || porto == null)
                throw new ArgumentException("Parametri sbagliati");

            _id = id;
            _lunghezzaMassima = lunghezzaMassima;
            _prezzoGiornaliero = prezzoGiornaliero;
            _porto = porto;

            _servizi = new List<Servizio>();
            _storico = new Storico();
        }

        public PostoBarca(string id, float lunghezzaMassima, IPorto porto) : this(id, lunghezzaMassima, 0, porto) { }
       
        public string Id
        {
            get
            {
                return _id;
            }
        }

        public float LunghezzaMassima
        {
            get
            {
                return _lunghezzaMassima;
            }
        }

        public float PrezzoGiornaliero
        {
            get
            {
                return _prezzoGiornaliero;
            }

            set
            {
                if (value <= 0)
                    throw new ArgumentException("Stai impostando un prezzo sbagliato!");
                _prezzoGiornaliero = value;
            }
        }

        public List<Servizio> Servizi
        {
            get
            {
                return _servizi;
            }
        }

        public Storico Storico
        {
            get
            {
                return _storico;
            }
        }

        public IPorto Porto
        {
            get
            {
                return _porto;
            }
        }

        public bool VerificaDisponibilita(DateTime from, DateTime to)
        {
            if (from == null || to == null || from > to)
                throw new ArgumentException("Intervallo non valido!");

            foreach (Prenotazione p in Storico.Prenotazioni)
            {
                if (p.Overlaps(from, to)) return false;
            }
            return true;
        }

        public bool Prenota(Barca b, DateTime from , DateTime to)
        {
            if (b == null || from == null || to == null || b.Lunghezza > LunghezzaMassima || from > to)
                throw new ArgumentException("Uno dei parametri di creazione è illegale!");
            if (!VerificaDisponibilita(from, to))
                return false;

            /*Id randomizzato con seed sempre diverso*/
            Random r = new Random(DateTime.Now.Millisecond);
            
            Prenotazione p = new Prenotazione("ID" + r.Next(1000000), from, to, b, this);
            /** Dopo che la creo la aggiungo allo storico, al suo interno la prenotazione comunque mantiene la coerenza*/
            Storico.Prenotazioni.Add(p);

            if (PrenotazioneAdded != null)
                PrenotazioneAdded(p, EventArgs.Empty);

            return true;
        }

        public bool RimuoviPrenotazione(Prenotazione p)
        {
            if (p == null || p.PostoBarca.Id != Id || p.PostoBarca.Porto.Nome != Porto.Nome)
                throw new ArgumentException("Uno dei parametri di inizializzazione non è valido!");
            Storico.Prenotazioni.Remove(p);
            if (PrenotazioneRemoved != null)
                PrenotazioneRemoved(p, new EventArgs());

            return true;
        }

        public override string ToString() {
            return "" + _id;
        }


        public override bool Equals(object obj)
        {
            if (!(obj is PostoBarca)) return false;
            else
            {
                PostoBarca toCompare = (PostoBarca)obj;
                if (toCompare.Id == Id && Porto.Nome == toCompare.Porto.Nome)
                    return true;
                else return false;
            }
        }
    }
}
