﻿using System;
using Porto.Model.Interfacce;

namespace Porto.Model
{
    [Serializable]
    public class Barca
    {
        private string _nome;
        private string _targa;
        private readonly float _lunghezza;
        private ICliente _proprietario;

        public Barca(string nome, string targa,float lunghezza)
        {
            if (string.IsNullOrEmpty(nome) || string.IsNullOrEmpty(targa) || lunghezza <= 0)
                throw new ArgumentException("Uno dei parametri di creazione non è valido!");
            _nome = nome;
            _targa = targa;
            _lunghezza = lunghezza;
        }

        public string Nome
        {
            get
            {
                return _nome;
            }
            set {
                _nome = value;
            }
        }

        public string Targa
        {
            get
            {
                return _targa;
            }
            set {
                _targa = value;
            }
        }

        public float Lunghezza
        {
            get
            {
                return _lunghezza;
            }
        }

        public ICliente Proprietario
        {
            get
            {
                return _proprietario;
            }

            set
            {
                if (value == null)
                    throw new ArgumentException("Stai cercando di impostare un proprietario nullo!");
                _proprietario = value;
            }
        }

        public override string ToString() {
            return _nome + " (" + _targa + ")";
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Barca))
                return false;
            else
            {
                Barca toCompare = (Barca)obj;
                if (toCompare.Targa == Targa)
                    return true;
                else return false;
            }
        }
    }
}
