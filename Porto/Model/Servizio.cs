﻿using System;

namespace Porto.Model
{
    [Serializable]
    public class Servizio
    {
        private readonly string _descrizione;

        public Servizio(string descrizione)
        {
            if (string.IsNullOrEmpty(descrizione))
                throw new ArgumentException("La descrizione è vuota!");
            _descrizione = descrizione;
        }

        public string Descrizione
        {
            get
            {
                return _descrizione;
            }
        }

        public override string ToString() {
            return _descrizione;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Servizio)) return false;
            else
            {
                Servizio toCompare = (Servizio)obj;
                if (toCompare.Descrizione == Descrizione)
                    return true;
                else return false;
            }
        }
    }
}
