﻿using System;
using System.Collections.Generic;
using Porto.Model.Interfacce;

namespace Porto.Model
{
    [Serializable]
    public class Attività
    {
        private string _descrizione;
        private readonly DateTime _dataCreazione;

        private readonly List<IAssistenteDiTerra> _operatori;
        private readonly Segnalazione _segnalazione;

        public Attività(string descrizione,List<IAssistenteDiTerra> operatori,Segnalazione s)
        {
            if (string.IsNullOrEmpty(descrizione) || operatori == null || s == null)
                throw new ArgumentException("I parametri di inizializzazione non sono validi!");
            if (operatori.Count < 1)
                throw new ArgumentException("Deve esserci almeno un assistente di terra!");
            _descrizione = descrizione;
            _operatori = operatori;
            _segnalazione = s;
            _dataCreazione = DateTime.Now;
        }

        public string Descrizione
        {
            get
            {
                return _descrizione;
            }

            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentException("La descrizione non è valida!");
                _descrizione = value;
            }
        }

        public DateTime DataCreazione
        {
            get
            {
                return _dataCreazione;
            }
        }

        public List<IAssistenteDiTerra> Operatori
        {
            get
            {
                return _operatori;
            }

           
        }

        public Segnalazione Segnalazione
        {
            get
            {
                return _segnalazione;
            }
        }

        public override string ToString() {
            return "" + _dataCreazione;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Attività)) return false;
            else
            {
                Attività toCompare = (Attività)obj;

                if (toCompare.Segnalazione.Equals(Segnalazione))
                    return true;
                else return false;
            }
        }
    }
}
