﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Porto.Model.Interfacce
{
    
    public interface IAttività
    {
        String Descrizione { get; }
        DateTime DataCreazione { get; }
    }
}
