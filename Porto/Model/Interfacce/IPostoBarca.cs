﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Porto.Model.Interfacce
{
    public interface IPostoBarca
    {
        String Id { get; }
        float LunghezzaMassima { get; }
        float PrezzoGiornaliero { get; }
        List<Servizio> Servizi { get; }
        IPorto Porto { get; }
    }
}
