﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Porto.Model.Interfacce
{
    public interface IAssistenteDiTerra
    {
        String Nome { get; }
        String Cognome { get; }
        String CodiceFiscale { get; }
        String NomeCognome { get; }
    }
}
