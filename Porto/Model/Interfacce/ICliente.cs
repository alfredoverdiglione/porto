﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Porto.Model.Interfacce
{
    public interface ICliente
    {
        String Nome { get; }
        String Cognome { get; }
        String CodiceFiscale { get; }    
    }
}
