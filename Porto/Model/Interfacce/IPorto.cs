﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Porto.Model;

namespace Porto.Model.Interfacce
{
    public interface IPorto
    {
        String Nome { get; }
        String Indirizzo { get; }
        List<Segnalazione> Segnalazioni { get; }
    }
}
