﻿using Porto.Model.Utenti;
using System.Collections.Generic;
using System;
using System.Linq;

namespace Porto.Model
{
    [Serializable]
    public class Storico
    {
        
        private readonly List<Prenotazione> _prenotazioni;

        public Storico()
        {
            _prenotazioni = new List<Prenotazione>();
        }

        public List<Prenotazione> Prenotazioni
        {
            get
            {
                return _prenotazioni;
            }
        }

        public List<Prenotazione> Filtra(Cliente c)
        {
            if (c == null)
                throw new ArgumentException("Il cliente non può essere nullo!");
            List<Prenotazione> result = new List<Prenotazione>();
            foreach (Prenotazione p in Prenotazioni)
            {
                if (p.Barca.Proprietario.Equals(c)) result.Add(p);
            }

            return result;
        }

        public List<Prenotazione> Filtra(DateTime from, DateTime to)
        {
            if (from == null || to == null || from > to )
                throw new ArgumentException("Parametri non validi!");
            List<Prenotazione> result = new List<Prenotazione>();
            foreach (Prenotazione p in Prenotazioni)
            {
                if (from<=p.DataInizio && p.DataFine<= to) result.Add(p);
            }

            return result;
        }
        public List<Prenotazione> Filtra(StatoPrenotazione s)
        {
           
            List<Prenotazione> result = new List<Prenotazione>();
            foreach (Prenotazione p in Prenotazioni)
            {
                if (p.StatoPrenotazione == s) result.Add(p);
            }

            return result;
        }
        public List<Prenotazione> Filtra(float prezzoMin, float prezzoMax)
        {
            if (prezzoMin <0 || prezzoMax <0 || prezzoMin>prezzoMax )
                throw new ArgumentException("Range non valido di prezzi!");
            List<Prenotazione> result = new List<Prenotazione>();
            foreach (Prenotazione p in Prenotazioni)
            {
                if (prezzoMin <= p.Prezzo && p.Prezzo <= prezzoMax) result.Add(p);
            }

            return result;
        }
        public List<Prenotazione> Filtra(int nGiorniMin,int nGiorniMax)
        {
            if (nGiorniMin<0 || nGiorniMin>nGiorniMax)
                throw new ArgumentException("Intervallo di filtraggio non valido!");
            List<Prenotazione> result = new List<Prenotazione>();
            foreach (Prenotazione p in Prenotazioni)
            {
                if (nGiorniMin<= p.NumeroGiorni && p.NumeroGiorni <= nGiorniMax) result.Add(p);
            }

            return result;
        }

        public List<Prenotazione> Filtra (params List<Prenotazione>[] liste)
        {
            if (liste == null)
                throw new ArgumentException("Cosa mi stai passando?");
            List<Prenotazione> result = new List<Prenotazione>();
            if (liste.Length == 0)
                throw new ArgumentException();
            if (liste.Length == 1)
                return liste[0];

            result = liste[0];
            foreach (List<Prenotazione> lista in liste)
                result = result.Intersect(lista).ToList();
            
            return result;
        }

    }
}