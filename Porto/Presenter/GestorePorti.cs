﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Porto.Persistence.Interfacce;
using Porto.Persistence;
using Porto.Model;
using Porto.Model.Utenti;
using System.Timers;

namespace Porto.Presenter
{
    public class GestorePorti
    {
        private static readonly String  fileName = "porti.dat";
        private readonly PersistenzaPorto _persistenza;
        private List<Model.Porto> _portiGestiti;
        private static GestorePorti _actualGestore = null;
        private readonly Timer _timer;

        private GestorePorti()
        {
            _persistenza = PersistenceFlyweightFactory.GetPersister(PersistenceType.Binary);
            LoadPorti();
            RefreshStatusPorti();
            SavePorti();

            /*Genero un evento ogni secondo*/
            _timer = new Timer(1000);
            _timer.Elapsed += OnElapsed;
            _timer.Enabled = true;
        }

        public static GestorePorti GetInstance()
        {
            if (_actualGestore == null) _actualGestore = new GestorePorti();
            return _actualGestore;
        }

        public List<Model.Porto> PortiGestiti
        {
            get
            {
                return _portiGestiti;
            }
        }

        private void LoadPorti()
        {
            _portiGestiti = _persistenza.LoadPorti(fileName);
        }

        /** Da chiamare tutte le volte che si agisce su un qualsiasi porto o suo elemento e
            si vuole mantenere persistenti queste modifiche*/
        public void SavePorti()
        {
            _persistenza.SavePorti(_portiGestiti, fileName);
        }

        private void RefreshStatusPorti()
        {
            foreach(Model.Porto p in PortiGestiti)
            {
                foreach(PostoBarca pb in p.PostiBarca)
                {
                    foreach(Prenotazione prenotazione in pb.Storico.Prenotazioni.Reverse<Prenotazione>())
                    {
                        if ((DateTime.Now - prenotazione.DataInizio).Days >= 1 &&
                            prenotazione.StatoPrenotazione == StatoPrenotazione.NonConfermata)
                            pb.Storico.Prenotazioni.Remove(prenotazione);
                    }
                }
            }
        }

        private void OnElapsed(Object source, ElapsedEventArgs e)
        {
            if (DateTime.Now.Hour == 0 && DateTime.Now.Minute == 0 && DateTime.Now.Second == 0)
                RefreshStatusPorti();
        }

        public List<Prenotazione> OttieniTuttePrenotazioniPerCliente(Cliente p)
        {
            if (p == null)
                throw new ArgumentException("Non puoi passare un Cliente nullo!");
            List<Prenotazione> result = new List<Prenotazione>();
            foreach(Model.Porto porto in PortiGestiti)
            {
                foreach(PostoBarca pb in porto.PostiBarca)
                {
                    List<Prenotazione> prenotazioniPostoBarca = pb.Storico.Filtra(p);
                    if (prenotazioniPostoBarca.Count > 0)
                        result.Concat<Prenotazione>(prenotazioniPostoBarca);
                }
            }
            return result;
        }

        public Model.Porto OttieniPortoPerDipendente(Dipendente dip)
        {
            if (dip == null) throw new ArgumentException("Il dipendente deve esistere!");
            foreach (Model.Porto porto in PortiGestiti)
            {
                if(porto.Nome == dip.LavoraPresso.Nome &&
                    porto.Indirizzo == dip.LavoraPresso.Indirizzo)
                    return porto;
            }
            /**Se non lo trovo vuol dire che non lavora presso nessun porto!*/
            return null;
        }


    }
}
