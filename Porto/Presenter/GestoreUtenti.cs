﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Porto.Persistence.Interfacce;
using Porto.Persistence;
using Porto.Model.Utenti;

namespace Porto.Presenter
{
    public class GestoreUtenti
    {
        private static readonly String fileName = "utenti.dat";
        private readonly PersistenzaUtente _persistenza;
        private List<Utente> _utentiGestiti;
        private static GestoreUtenti _actualGestore = null;

        public List<Utente> UtentiGestiti
        {
            get
            {
                return _utentiGestiti;
            }
        }

        private GestoreUtenti()
        {
            _persistenza = PersistenceFlyweightFactory.GetPersister(PersistenceType.Binary);
            LoadUtenti();
            SaveUtenti();
        
        }

        public static GestoreUtenti GetInstance()
        {
            if (_actualGestore == null) _actualGestore = new GestoreUtenti();
            return _actualGestore;
        }

        private void LoadUtenti()
        {
            _utentiGestiti = _persistenza.LoadUtenti(fileName);
        }

        public void SaveUtenti()
        {
            _persistenza.SaveUtenti(_utentiGestiti, fileName);
        }

    }
}
